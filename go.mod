module gitlab.oit.duke.edu/devil-ops/planisphere-sdk

go 1.23

require (
	github.com/google/go-querystring v1.1.0
	github.com/google/uuid v1.6.0
	github.com/spf13/cobra v1.8.1
	github.com/stretchr/testify v1.8.1
	moul.io/http2curl/v2 v2.3.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/go-cmp v0.5.8 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/kr/pretty v0.3.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rogpeppe/go-internal v1.9.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
