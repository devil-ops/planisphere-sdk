package main

import (
	"log"
	"os"
	"strconv"

	"gitlab.oit.duke.edu/devil-ops/planisphere-sdk/planisphere"
)

func main() {
	username, _ := os.LookupEnv("PLANISPHERE_USERNAME")
	token, _ := os.LookupEnv("PLANISPHERE_TOKEN")

	if len(os.Args) != 2 {
		log.Fatalf("Usage: %s DEVICE_ID", os.Args[0])
	}
	c := planisphere.NewClient(username, token, nil)
	deviceID, _ := strconv.Atoi(os.Args[1])
	r, _, err := c.Device.Vulnerabilities(nil, deviceID)
	if err != nil {
		panic(err)
	}
	for _, item := range *r {
		log.Printf("%+v", item)
	}
}
