package main

import (
	"os"
	"time"

	"github.com/apex/log"

	"gitlab.oit.duke.edu/devil-ops/planisphere-sdk/planisphere"
)

func main() {
	log.SetLevel(log.DebugLevel)
	key, _ := os.LookupEnv("PLANISPHEREREPORT_KEY")
	if key == "" {
		log.Fatal("Please set PLANISPHEREREPORT_KEY env to test")
	}

	payload := &planisphere.SelfReportPayload{}
	payload.Data.Serial = "C78771ED-27E7-4B4C-B9FF-34B421A3A89A"
	payload.LastActive = time.Now()
	payload.ExtraData = map[string]string{"what_is_this": "Fake device generated from planisphere-selfreport-go..."}
	softwareTable := [][]string{}
	fakeSoftware := []string{"foo-software", "0.0.1"}
	softwareTable = append(softwareTable, fakeSoftware)
	payload.Data.InstalledSoftware = softwareTable
	// osid := [][]string{{"foo", "bar"}}
	osid := map[string]string{
		"baz": "zing!",
	}
	payload.Data.ExternalOSIdentifiers = osid

	payR, err := payload.SubmitTest(key)
	if err != nil {
		log.Fatalf("%+v", err)
	}
	log.Infof("Got the following test data back: %+v", payR)
}
