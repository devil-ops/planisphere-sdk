package main

import (
	"os"

	"github.com/apex/log"

	"gitlab.oit.duke.edu/devil-ops/planisphere-sdk/planisphere"
)

func main() {
	username, _ := os.LookupEnv("PLANISPHERE_USERNAME")
	token, _ := os.LookupEnv("PLANISPHERE_TOKEN")

	c := planisphere.NewClient(username, token, nil)

	depts, _, _ := c.Department.List(nil)
	log.Infof("%+v", depts)
}
