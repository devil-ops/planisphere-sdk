package main

import (
	"os"

	"github.com/apex/log"

	"gitlab.oit.duke.edu/devil-ops/planisphere-sdk/planisphere"
)

func main() {
	username, _ := os.LookupEnv("PLANISPHERE_USERNAME")
	token, _ := os.LookupEnv("PLANISPHERE_TOKEN")

	c := planisphere.NewClient(username, token, nil)

	r, _, _ := c.Subnet.List(nil)
	for _, item := range *r {
		log.Infof("%v", item)
	}
}
