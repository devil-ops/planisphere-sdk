package main

import (
	"os"
	"strconv"
	"time"

	"github.com/apex/log"

	"gitlab.oit.duke.edu/devil-ops/planisphere-sdk/planisphere"
)

func main() {
	username, _ := os.LookupEnv("PLANISPHERE_USERNAME")
	token, _ := os.LookupEnv("PLANISPHERE_TOKEN")

	if len(os.Args) != 2 {
		log.Fatal("Usage script NAME_PATTERN")
	}

	c := planisphere.NewClient(username, token, nil)
	p := &planisphere.DeviceParams{}
	log.Infof("Printing all devices matching %v", os.Args[1])
	t := time.Now().Unix()
	yesterday := t - 86400
	log.Infof("%v - %v", t, yesterday)
	p.ActiveSince = strconv.Itoa(int(yesterday))

	r, _, _ := c.Device.List(nil, p)
	for _, item := range *r {
		log.Infof("%+v", item.Names)
	}
}
