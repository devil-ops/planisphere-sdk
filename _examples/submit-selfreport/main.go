package main

import (
	"os"
	"time"

	"github.com/apex/log"

	"gitlab.oit.duke.edu/devil-ops/planisphere-sdk/planisphere"
)

func main() {
	key, _ := os.LookupEnv("PLANISPHEREREPORT_KEY")
	if key == "" {
		log.Fatal("Please set PLANISPHEREREPORT_KEY env to test")
	}

	payload := &planisphere.SelfReportPayload{}
	payload.Data.Serial = "C78771ED-27E7-4B4C-B9FF-34B421A3A89A"
	payload.LastActive = time.Now()
	payload.ExtraData = map[string]string{"what_is_this": "Fake device generated from planisphere-selfreport-go..."}
	softwareTable := [][]string{}
	fakeSoftware := []string{"foo-software", "0.0.1"}
	softwareTable = append(softwareTable, fakeSoftware)
	payload.Data.InstalledSoftware = softwareTable

	err := payload.Submit(key)
	if err != nil {
		log.Fatalf("%v", err)
	}
	log.Info("Submitted ❤️")
}
