package main

import (
	"log"
	"os"
	"strconv"
	"time"

	"github.com/drewstinnett/gout/v2"
	"gitlab.oit.duke.edu/devil-ops/planisphere-sdk/planisphere"
)

func main() {
	username, _ := os.LookupEnv("PLANISPHERE_USERNAME")
	token, _ := os.LookupEnv("PLANISPHERE_TOKEN")

	c := planisphere.NewClient(username, token, nil)
	p := &planisphere.DeviceParams{}
	if len(os.Args) == 1 {
		log.Print("Printing all devices")
		t := time.Now().Unix()
		yesterday := t - 86400
		log.Printf("%v - %v", t, yesterday)
		p.ActiveSince = strconv.Itoa(int(yesterday))

		r, _, _ := c.Device.List(nil, p)
		for _, item := range *r {
			gout.MustPrint(item)
		}
	} else {
		p.ShowAll = true

		deviceID, _ := strconv.Atoi(os.Args[1])
		r, _, _ := c.Device.Get(nil, deviceID, p)
		gout.MustPrint(r)
	}
}
