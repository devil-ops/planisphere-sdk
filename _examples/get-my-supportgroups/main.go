package main

import (
	"os"

	"github.com/apex/log"

	"gitlab.oit.duke.edu/devil-ops/planisphere-sdk/planisphere"
)

func main() {
	username, _ := os.LookupEnv("PLANISPHERE_USERNAME")
	token, _ := os.LookupEnv("PLANISPHERE_TOKEN")

	c := planisphere.NewClient(username, token, nil)

	r, _, err := c.SupportGroup.ListKeysByUser(nil, username)
	if err != nil {
		log.Fatalf("%v", err)
	}
	for _, item := range r {
		log.Infof("%v", item)
	}

	idR, _, err := c.SupportGroup.ListIDsByUser(nil, username)
	if err != nil {
		log.Fatalf("%v", err)
	}
	for _, idItem := range idR {
		log.Infof("%v", idItem)
	}
}
