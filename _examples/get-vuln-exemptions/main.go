package main

import (
	"context"
	"fmt"
	"os"

	"github.com/drewstinnett/gout/v2"
	"gitlab.oit.duke.edu/devil-ops/planisphere-sdk/planisphere"
)

func main() {
	username, _ := os.LookupEnv("PLANISPHERE_USERNAME")
	token, _ := os.LookupEnv("PLANISPHERE_TOKEN")

	c := planisphere.NewClient(username, token, nil)
	subnets, _, err := c.ScanExemptions.Subnets(context.Background())
	panicIfErr(err)
	fmt.Println("Exempt Subnets:")
	gout.MustPrint(subnets)

	wirelessSubnets, _, err := c.ScanExemptions.WirelessSubnets(context.Background())
	panicIfErr(err)
	fmt.Println("Exempt Wireless Subnets:")
	gout.MustPrint(wirelessSubnets)

	ips, _, err := c.ScanExemptions.StaticIPs(context.Background())
	panicIfErr(err)
	fmt.Println("Exempt Static IPs:")
	gout.MustPrint(ips)
}

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}
