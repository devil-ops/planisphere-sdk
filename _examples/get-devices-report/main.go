package main

import (
	"os"

	"github.com/apex/log"

	"gitlab.oit.duke.edu/devil-ops/planisphere-sdk/planisphere"
)

func main() {
	username, _ := os.LookupEnv("PLANISPHERE_USERNAME")
	token, _ := os.LookupEnv("PLANISPHERE_TOKEN")

	if len(os.Args) != 2 {
		log.Fatalf("Usage: %s REPORT_STRING", os.Args[0])
	}
	c := planisphere.NewClient(username, token, nil)
	r, _, _ := c.Device.Report(nil, os.Args[1])
	for _, item := range *r {
		log.Infof("%+v", item)
	}
}
