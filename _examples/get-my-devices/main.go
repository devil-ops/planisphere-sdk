package main

import (
	"os"

	"github.com/apex/log"

	"gitlab.oit.duke.edu/devil-ops/planisphere-sdk/planisphere"
)

func main() {
	username, _ := os.LookupEnv("PLANISPHERE_USERNAME")
	token, _ := os.LookupEnv("PLANISPHERE_TOKEN")

	c := planisphere.NewClient(username, token, nil)

	supportGroupIDs, _, err := c.SupportGroup.ListIDsByUser(nil, username)
	if err != nil {
		log.Fatalf("%v", err)
	}
	p := &planisphere.ReportParams{
		SupportGroupIDs: supportGroupIDs,
		VRFs:            []string{"wireless"},
		// Names:           "oit.duke.edu",
		// DepartmentKeys:  []string{"SSI:OIT"},
	}
	log.Infof("%+v", p)
	devices, _, err := c.Report.List(nil, p)
	if err != nil {
		log.Fatalf("%v", err)
	}
	for _, item := range *devices {
		log.Infof("%v - %v", item.URL, item.Names)
	}
}
