package main

import (
	"context"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/drewstinnett/gout/v2"
	"gitlab.oit.duke.edu/devil-ops/planisphere-sdk/planisphere"
)

func main() {
	username, _ := os.LookupEnv("PLANISPHERE_USERNAME")
	token, _ := os.LookupEnv("PLANISPHERE_TOKEN")

	c := planisphere.NewClient(username, token, nil)
	p := &planisphere.DeviceParams{
		ShowVulnerabilities: true,
	}
	if len(os.Args) == 1 {
		log.Print("Printing all devices")
		t := time.Now().Unix()
		yesterday := t - 86400
		log.Printf("%v - %v", t, yesterday)
		p.ActiveSince = strconv.Itoa(int(yesterday))

		r, _, err := c.Device.List(context.Background(), p)
		if err != nil {
			panic(err)
		}
		for _, item := range *r {
			log.Printf("%v", item)
		}
	} else {
		p.ShowAll = true

		deviceID, _ := strconv.Atoi(os.Args[1])
		r, _, err := c.Device.Get(context.Background(), deviceID, p)
		if err != nil {
			panic(err)
		}
		gout.MustPrint(r)
		// log.Printf("%+v", r)
	}
}
