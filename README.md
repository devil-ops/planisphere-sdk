[![coverage report](https://gitlab.oit.duke.edu/devil-ops/planisphere-sdk/badges/main/coverage.svg)](https://gitlab.oit.duke.edu/devil-ops/planisphere-sdk/-/commits/main)
[![pipeline status](https://gitlab.oit.duke.edu/devil-ops/planisphere-sdk/badges/main/pipeline.svg)](https://gitlab.oit.duke.edu/devil-ops/planisphere-sdk/-/commits/main)
[![Go Reference](https://pkg.go.dev/badge/gitlab.oit.duke.edu/devil-ops/planisphere-sdk.svg)](https://pkg.go.dev/gitlab.oit.duke.edu/devil-ops/planisphere-sdk)

# Planisphere SDK

Golang SDK for Planisphere API

Check out the [examples](examples/) directory!

See [Go
Reference](https://pkg.go.dev/gitlab.oit.duke.edu/devil-ops/planisphere-sdk) for
full documentation

## Usage

```go
    // Pull in your appropriate credentials
    username, err := os.LookupEnv("PLANISPHERE_USERNAME")
    checkErr(err)
    token, err := os.LookupEnv("PLANISPHERE_TOKEN")
    checkErr(err)

    // Use the client to interact
    c := planisphere.NewClient(username, token, nil)
```

*This code is freely available for non-commercial use and is provided as-is with no warranty.*
