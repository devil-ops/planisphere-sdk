package planisphere

import (
	"io"
	"net/http"

	"github.com/google/go-querystring/query"
)

func containsString(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

// mustNewRequest is a wrapper around http.NewRequest that panics if an error
// occurs
func mustNewRequest(method, url string, body io.Reader) *http.Request {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		panic(err)
	}
	return req
}

func mustNewGetRequest(url string) *http.Request {
	return mustNewRequest("GET", url, nil)
}

func mustQueryValues(params interface{}) string {
	urlParms, err := query.Values(params)
	if err != nil {
		panic(err)
	}
	return urlParms.Encode()
}

func dclose(c io.Closer) {
	if err := c.Close(); err != nil {
		panic(err)
	}
}

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}
