package planisphere

import (
	"context"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/spf13/cobra"
	"github.com/stretchr/testify/require"
)

func TestDevicesReportList(t *testing.T) {
	got, _, err := c.Report.Generate(context.TODO(), &ReportParams{})
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestDevicesReportListErr(t *testing.T) {
	tsrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(429)
	}))
	tc := NewClient("", "", nil)
	u, err := url.Parse(tsrv.URL)
	panicIfErr(err)
	tc.BaseURL = u

	got, _, err := tc.Report.Generate(context.TODO(), &ReportParams{})
	require.Error(t, err)
	require.Nil(t, got)
}

func TestNewReportParamsWithCmd(t *testing.T) {
	cmd := &cobra.Command{}
	got := c.NewReportParams(WithCmd(*cmd))
	require.NotNil(t, got)
}
