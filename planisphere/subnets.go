package planisphere

import (
	"context"
	"fmt"
	"strings"
)

// Subnet is what Planisphere calls a Subnet
type Subnet struct {
	Canonical                string        `json:"canonical,omitempty"`
	Department               *Department   `json:"department,omitempty"`
	ExcludedFromEndpointMgmt bool          `json:"excluded_from_endpoint_mgmt,omitempty"`
	GuestNetwork             bool          `json:"guest_network,omitempty"`
	Name                     string        `json:"name,omitempty"`
	SupportGroup             *SupportGroup `json:"support_group,omitempty"`
	VRFs                     *VRFList      `json:"vrfs,omitempty"`
}

// VRF is a vrf
type VRF struct {
	Department               *Department   `json:"department,omitempty"`
	ExcludedFromCrowdstrike  bool          `json:"excluded_from_crowdstrike,omitempty"`
	ExcludedFromEndpointMgmt bool          `json:"excluded_from_endpoint_mgmt,omitempty"`
	GuestNetwork             bool          `json:"guest_network,omitempty"`
	Name                     string        `json:"name,omitempty"`
	SupportGroup             *SupportGroup `json:"support_group,omitempty"`
}

// VRFList represents multiple VRFs
type VRFList []VRF

// MarshalCSV handles marshaling for CSV encoding
func (v VRFList) MarshalCSV() ([]byte, error) {
	names := make([]string, len(v))
	for idx, item := range v {
		names[idx] = item.Name
	}
	return []byte(strings.Join(names, ", ")), nil
}

// MarshalCSV handles marshaling for CSV encoding
func (v VRF) MarshalCSV() ([]byte, error) {
	return []byte(v.Name), nil
}

// SubnetList is a list of Subnets
type SubnetList []Subnet

const (
	subnetPath = "/api/v1/subnets"
)

// SubnetService is the interface to subnet methods
type SubnetService interface {
	List(context.Context) (*SubnetList, *Response, error)
}

// SubnetServiceOp is the operator for the SubnetService
type SubnetServiceOp struct {
	client *Client
}

// List lists out all subnets known to Planisphere
func (svc *SubnetServiceOp) List(_ context.Context) (*SubnetList, *Response, error) {
	req := mustNewGetRequest(fmt.Sprintf("%s%s", svc.client.BaseURL, subnetPath))
	var err error
	r := &SubnetList{}
	resp := &Response{}

	resp.Response, err = svc.client.sendRequest(req, &r) // nolint:golint,bodyclose
	if err != nil {
		return nil, resp, err
	}
	err = resp.Response.Body.Close()
	if err != nil {
		return nil, resp, err
	}
	return r, resp, nil
}
