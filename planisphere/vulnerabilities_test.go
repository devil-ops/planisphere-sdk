package planisphere

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestBadCriticality(t *testing.T) {
	_, err := GetVulnerabilityCriticality("NeverExists")
	if err == nil {
		t.Fatalf("Checking for a non existent Criticality did not error")
	}
}

func TestGoodCriticality(t *testing.T) {
	val, _ := GetVulnerabilityCriticality("Critical")
	if val != VulnCritical {
		t.Fatalf("Critical value was not 50")
	}
}

func TestGoodLowercaseCriticality(t *testing.T) {
	val, _ := GetVulnerabilityCriticality("critical")
	if val != VulnCritical {
		t.Fatalf("Critical lowercase value was not 50")
	}
}

func TestVulnerabilityCriticalities(t *testing.T) {
	got := VulnerabilityCriticalityNames()
	require.NotNil(t, got)
	require.Contains(t, got, "high")
}

func TestActionabilityBool(t *testing.T) {
	require.EqualValues(t, Actionable.ReqBool(), boolPTR(true))
	require.EqualValues(t, UnActionable.ReqBool(), boolPTR(false))
	// require.EqualValues(t, AnyActionability.Bool(), nil)
}

func TestActionabilities(t *testing.T) {
	require.Contains(t, GetActionabilities(), AnyActionability)
	require.Contains(t, GetActionabilities(), UnActionable)
	require.Contains(t, GetActionabilities(), UnActionable)
}

func TestVulnFilterActionability(t *testing.T) {
	require.Equal(t, true, VulnFilterActionability(VulnFilterParams{Actionability: Actionable}, Vulnerability{Actionable: true}))
	require.Equal(t, false, VulnFilterActionability(VulnFilterParams{Actionability: UnActionable}, Vulnerability{Actionable: true}))
	require.Equal(t, true, VulnFilterActionability(VulnFilterParams{Actionability: AnyActionability}, Vulnerability{Actionable: true}))
	require.Equal(t, true, VulnFilterActionability(VulnFilterParams{Actionability: AnyActionability}, Vulnerability{Actionable: false}))
}

func TestVulnFilterRisk(t *testing.T) {
	require.Equal(t, true, VulnFilterRisk(VulnFilterParams{Risk: AcceptedRisk}, Vulnerability{AcceptedRisk: true}))
	require.Equal(t, false, VulnFilterRisk(VulnFilterParams{Risk: AcceptedRisk}, Vulnerability{AcceptedRisk: false}))
	require.Equal(t, true, VulnFilterRisk(VulnFilterParams{Risk: UnAcceptedRisk}, Vulnerability{AcceptedRisk: false}))
	require.Equal(t, true, VulnFilterRisk(VulnFilterParams{Risk: AnyRisk}, Vulnerability{AcceptedRisk: true}))
	require.Equal(t, true, VulnFilterRisk(VulnFilterParams{Risk: AnyRisk}, Vulnerability{AcceptedRisk: false}))
}

func TestVulnFilterResponsibility(t *testing.T) {
	require.Equal(t, true, VulnFilterResponsibility(VulnFilterParams{Responsibility: OSResponsibility}, Vulnerability{ResponsibilityType: "os"}))
	require.Equal(t, true, VulnFilterResponsibility(VulnFilterParams{Responsibility: AppResponsibility}, Vulnerability{ResponsibilityType: "app"}))
	require.Equal(t, true, VulnFilterResponsibility(VulnFilterParams{Responsibility: AnyResponsibility}, Vulnerability{ResponsibilityType: "app"}))
	require.Equal(t, true, VulnFilterResponsibility(VulnFilterParams{Responsibility: AnyResponsibility}, Vulnerability{ResponsibilityType: "os"}))
}
