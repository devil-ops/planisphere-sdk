package planisphere

import (
	"context"
	"fmt"
)

// Department represents a department with a key and a name
type Department struct {
	Key  string `json:"key,omitempty"`
	Name string `json:"name,omitempty"`
}

// MarshalCSV handles marshaling for CSV encoding
func (d Department) MarshalCSV() ([]byte, error) {
	return []byte(d.Key), nil
}

// DepartmentList is a...list of Departments
type DepartmentList []Department

const (
	departmentPath = "/api/v1/departments"
)

// DepartmentService is an interface to the Department methods
type DepartmentService interface {
	List(context.Context) (*DepartmentList, *Response, error)
}

// DepartmentServiceOp is the operator for the DepartmentService
type DepartmentServiceOp struct {
	client *Client
}

// List returns a list of all departments
func (svc *DepartmentServiceOp) List(_ context.Context) (*DepartmentList, *Response, error) {
	req := mustNewGetRequest(fmt.Sprintf("%s%s", svc.client.BaseURL, departmentPath))
	var err error
	r := &DepartmentList{}
	resp := &Response{}

	// Not sure why the linter is firing here...🤔
	resp.Response, err = svc.client.sendRequest(req, &r) //nolint:golint,bodyclose
	if err != nil {
		return nil, resp, err
	}
	dclose(resp.Response.Body)
	return r, resp, nil
}
