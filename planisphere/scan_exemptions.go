package planisphere

import (
	"context"
	"fmt"
	"sort"
)

const (
	vulnExemptionPath = "/api/v1/vulnerability_scan_exemptions"
)

// ExemptStaticIPs defines a list of ip addresses exempt from security scans
type ExemptStaticIPs []string

// Strings returns a string representation of all the IP addresses
func (e ExemptStaticIPs) Strings() []string {
	sort.Strings(e)
	return e
}

// ExemptSubnets defines a list of subnets that are exempt from security scans
type ExemptSubnets []string

// ExemptWirelessSubnets defines a list of wireless subnets that are exempt from security scans
type ExemptWirelessSubnets []string

// AllExemptions represents all of the different exemption type data
type AllExemptions struct {
	StaticIPs       ExemptStaticIPs
	Subnets         ExemptSubnets
	WirelessSubnets ExemptWirelessSubnets
}

// All returns a string array of all exempted items
func (e AllExemptions) All() []string {
	r := []string{}
	r = append(r, e.StaticIPs...)
	r = append(r, e.Subnets...)
	r = append(r, e.WirelessSubnets...)
	sort.Strings(r)
	return r
}

// ScanExemptionsService is the interface that defines how the
// vulnerability_scan_exemptions calls behave
type ScanExemptionsService interface {
	StaticIPs(context.Context) (*ExemptStaticIPs, *Response, error)
	Subnets(context.Context) (*ExemptSubnets, *Response, error)
	WirelessSubnets(context.Context) (*ExemptWirelessSubnets, *Response, error)
	All(context.Context) (*AllExemptions, *Response, error)
}

// ScanExemptionsServiceOp is the operator for the ScanExemptions interface
type ScanExemptionsServiceOp struct {
	client *Client
}

// StaticIPs returns the list of exempted static ips
func (svc *ScanExemptionsServiceOp) StaticIPs(_ context.Context) (*ExemptStaticIPs, *Response, error) {
	req := mustNewGetRequest(fmt.Sprintf("%s%s/static_ips", svc.client.BaseURL, vulnExemptionPath))
	r := &ExemptStaticIPs{}
	resp := &Response{}
	var err error
	resp.Response, err = svc.client.sendRequest(req, &r) //nolint:golint,bodyclose
	if err != nil {
		return nil, resp, err
	}
	dclose(resp.Response.Body)

	return r, resp, nil
}

// Subnets returns the list of exempted subnets
func (svc *ScanExemptionsServiceOp) Subnets(_ context.Context) (*ExemptSubnets, *Response, error) {
	req := mustNewGetRequest(fmt.Sprintf("%s%s/subnets", svc.client.BaseURL, vulnExemptionPath))
	r := &ExemptSubnets{}
	resp := &Response{}
	var err error
	resp.Response, err = svc.client.sendRequest(req, &r) //nolint:golint,bodyclose
	if err != nil {
		return nil, resp, err
	}
	dclose(resp.Response.Body)

	return r, resp, nil
}

// WirelessSubnets returns a list of exempted wireless subnets
func (svc *ScanExemptionsServiceOp) WirelessSubnets(_ context.Context) (*ExemptWirelessSubnets, *Response, error) {
	req := mustNewGetRequest(fmt.Sprintf("%s%s/wireless_subnets", svc.client.BaseURL, vulnExemptionPath))
	r := &ExemptWirelessSubnets{}
	resp := &Response{}
	var err error
	resp.Response, err = svc.client.sendRequest(req, &r) //nolint:golint,bodyclose
	if err != nil {
		return nil, resp, err
	}
	dclose(resp.Response.Body)

	return r, resp, nil
}

// All return all exemptions
func (svc *ScanExemptionsServiceOp) All(ctx context.Context) (*AllExemptions, *Response, error) {
	r := &AllExemptions{}

	ips, resp, err := svc.StaticIPs(ctx)
	if err != nil {
		return nil, resp, err
	}
	subnets, resp, err := svc.Subnets(ctx)
	if err != nil {
		return nil, resp, err
	}
	wirelessSubnets, resp, err := svc.WirelessSubnets(ctx)
	if err != nil {
		return nil, resp, err
	}

	r.StaticIPs = *ips
	r.Subnets = *subnets
	r.WirelessSubnets = *wirelessSubnets

	return r, nil, nil
}
