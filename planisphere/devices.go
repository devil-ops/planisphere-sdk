package planisphere

import (
	"context"
	"fmt"
	"strings"
	"time"
)

const (
	devicePath = "/api/v1/devices"
)

// DeviceService is an interface to the Device methods
type DeviceService interface {
	Get(context.Context, int, *DeviceParams) (*Device, *Response, error)
	VulnerabilityDetections(context.Context, int64, *DeviceParams) (*VulnerabilityList, *Response, error)
	// ActionableVulnerabilities(context.Context, int) (*VulnerabilityList, *Response, error)
	List(context.Context, *DeviceParams) (*DeviceList, *Response, error)
	// Report(context.Context, string) (*DeviceList, *Response, error)
}

// DeviceServiceOp is the operator for the Service
type DeviceServiceOp struct {
	client *Client
}

// DeviceList is a type for multiple Devices
type DeviceList []Device

// Len needed to fulfill the sort.Interface
func (l DeviceList) Len() int {
	return len(l)
}

// Swap needed to fulfill the sort.Interface
func (l DeviceList) Swap(i, j int) {
	l[i], l[j] = l[j], l[i]
}

// Less needed to fulfill the sort.Interface
func (l DeviceList) Less(i, j int) bool {
	return strings.ToLower(l[i].PrimaryName) < strings.ToLower(l[j].PrimaryName)
}

// DeviceParams contains options for Device listings. Full information on these can be found at https://planisphere.oit.duke.edu/api_info
type DeviceParams struct {
	ActiveSince           string `url:"active_since,omitempty"`
	DepartmentDetails     bool   `url:"department_details,omitempty"`
	MacAddress            string `url:"mac_address,omitempty"`
	ShowAll               bool   `url:"show_all,omitempty"`
	ShowCustomFields      bool   `url:"show_custom_fields,omitempty"`
	ShowDataSourceRecords bool   `url:"show_data_source_records,omitempty"`
	ShowEndpointMgmtInfo  bool   `url:"show_endpoint_mgmt_info,omitempty"`
	ShowMacAddress        bool   `url:"show_mac_addresses,omitempty"`
	ShowNames             bool   `url:"show_names,omitempty"`
	ShowNulls             bool   `url:"show_nulls,omitempty"`
	ShowOses              bool   `url:"show_oses,omitempty"`
	ShowQuarantineReasons bool   `url:"show_quarantine_reason,omitempty"`
	ShowSSIDs             bool   `url:"show_ssids,omitempty"`
	ShowSubnets           bool   `url:"show_subnets,omitempty"`
	ShowSupportStaff      bool   `url:"show_support_staff,omitempty"`
	ShowVRFs              bool   `url:"show_vrfs,omitempty"`
	ShowVulnerabilities   bool   `url:"-"`
	// Actionable            *bool  `url:"-"`
	Actionability       Actionability `url:"-"`
	Risk                Risk          `url:"-"`
	ResponsibilityType  string        `url:"-"`
	SupportGroupDetails bool          `url:"support_group_details,omitempty"`
	SupportGroupKey     string        `url:"support_group_key,omitempty"`
	SupportGroupID      int           `url:"support_group_id,omitempty"`
	UserDetails         bool          `url:"user_details,omitempty"`
}

// vulnerabilities gets a listing of found vulnerabilities on a given device
func (svc *DeviceServiceOp) vulnerabilities(id int64) (*VulnerabilityList, *Response, error) {
	req := mustNewGetRequest(fmt.Sprintf("%s%s/%d/vulnerability_detections", svc.client.BaseURL, devicePath, id))
	r := &VulnerabilityList{}
	var err error
	resp := &Response{}

	resp.Response, err = svc.client.sendRequest(req, &r) //nolint:golint,bodyclose
	if err != nil {
		return nil, resp, err
	}
	dclose(resp.Response.Body)

	return r, resp, nil
}

// VulnerabilityDetections gets a listing of found vulnerabilities on a given device that are classified as actionable
func (svc *DeviceServiceOp) VulnerabilityDetections(_ context.Context, id int64, params *DeviceParams) (*VulnerabilityList, *Response, error) {
	all, resp, err := svc.vulnerabilities(id)
	if err != nil {
		return nil, resp, err
	}

	// Filter locally here until the API supports it
	f := VulnFilterParams{
		Actionability:  params.Actionability,
		Responsibility: Responsibility(params.ResponsibilityType),
		Risk:           params.Risk,
	}
	filtered := ApplyAllVulnFilters(f, *all)

	return &filtered, resp, nil
}

// Get returns information on a single device
func (svc *DeviceServiceOp) Get(ctx context.Context, id int, params *DeviceParams) (*Device, *Response, error) {
	urlParmsEncoded := mustQueryValues(params)
	req := mustNewGetRequest(fmt.Sprintf("%s%s/%d?%s", svc.client.BaseURL, devicePath, id, urlParmsEncoded))
	// root := new(registryRoot)
	r := &Device{}
	resp := &Response{}
	var err error

	resp.Response, err = svc.client.sendRequest(req, &r) //nolint:golint,bodyclose
	if err != nil {
		return nil, resp, err
	}
	dclose(resp.Response.Body)

	// Do we want to include vulnerabilities?
	if params.ShowVulnerabilities {
		vulns, _, err := svc.client.Device.VulnerabilityDetections(ctx, r.ID, params)
		if err != nil {
			return nil, nil, err
		}

		var filteredVulns VulnerabilityList
		for _, v := range *vulns {
			if v.Actionable {
				filteredVulns = append(filteredVulns, v)
			}
		}
		r.VulnerabilityDetections = &filteredVulns
	}
	return r, resp, nil
}

// List returns multiple devices based on the given parameters
func (svc *DeviceServiceOp) List(ctx context.Context, params *DeviceParams) (*DeviceList, *Response, error) {
	urlParmsEncoded := mustQueryValues(params)
	req := mustNewGetRequest(fmt.Sprintf("%s%s?%s", svc.client.BaseURL, devicePath, urlParmsEncoded))
	// root := new(registryRoot)
	r := &DeviceList{}
	resp := &Response{}
	var err error
	resp.Response, err = svc.client.sendRequest(req, &r) //nolint:golint,bodyclose
	if err != nil {
		return nil, resp, err
	}
	dclose(resp.Response.Body)

	// Do we also need to look up the Vulnerabilities?
	if params.ShowVulnerabilities {
		var hostsWithVulns DeviceList
		for _, device := range *r {
			vulns, _, err := svc.VulnerabilityDetections(ctx, device.ID, params)
			if err != nil {
				return nil, nil, err
			}
			device.VulnerabilityDetections = vulns
			hostsWithVulns = append(hostsWithVulns, device)
		}
		return &hostsWithVulns, resp, nil
	}
	return r, resp, nil
}

// DataSource represents a source of data for devices to feed from inside of Planisphere
type DataSource struct {
	Data struct {
		DeviceType   string  `json:"device_type,omitempty" yaml:"device_type,omitempty"`
		MacAddresses Strings `json:"mac_addresses,omitempty" yaml:"mac_addresses,omitempty"`
		Manufacturer string  `json:"manufacturer,omitempty" yaml:"manufacturer,omitempty"`
	} `json:"data,omitempty" yaml:"data,omitempty"`
	DataSourceID       float64        `json:"data_source_id,omitempty" yaml:"data_source_id,omitempty"`
	DataSourceName     string         `json:"data_source_name,omitempty" yaml:"data_source_name,omitempty"`
	DataSourcePriority float64        `json:"data_source_priority,omitempty" yaml:"data_source_priority,omitempty"`
	DataTimestamp      string         `json:"data_timestamp,omitempty" yaml:"data_timestamp,omitempty"`
	ExtraData          map[string]any `json:"extra_data,omitempty" yaml:"extra_data,omitempty" csv:"-"`
	Key                string         `json:"key,omitempty" yaml:"key,omitempty"`
	LastActive         string         `json:"last_active,omitempty" yaml:"last_active,omitempty"`
	LastUpdated        string         `json:"last_updated,omitempty" yaml:"last_updated,omitempty"`
	Legacy             bool           `json:"legacy" yaml:"legacy"`
	Name               string         `json:"name" yaml:"name"`
	Quarantined        bool           `json:"quarantined" yaml:"quarantined"`
	ReferencedObjectID float64        `json:"referenced_object_id,omitempty" yaml:"reference_object_id,omitempty"`
	SupportGroupLabel  interface{}    `json:"support_group_label,omitempty" yaml:"support_group_label,omitempty" csv:"-"`
	Type               string         `json:"type,omitempty" yaml:"type,omitempty"`
	URL                string         `json:"url,omitempty" yaml:"url,omitempty"`
}

// Os is an Operating System
type Os struct {
	DiskEncrypted   bool       `json:"disk_encrypted,omitempty" yaml:"disk_encrypted,omitempty"`
	Hostname        string     `json:"hostname,omitempty" yaml:"hostname,omitempty"`
	LastActive      *time.Time `json:"last_active,omitempty" yaml:"last_active,omitempty"`
	OsFamily        string     `json:"os_family,omitempty" yaml:"os_family,omitempty"`
	OsFullname      string     `json:"os_fullname,omitempty" yaml:"os_fullname,omitempty"`
	OsName          string     `json:"os_name,omitempty" yaml:"os_name,omitempty"`
	UnsupportedDate string     `json:"unsupported_date,omitempty" yaml:"unsupported_date,omitempty"`
	// UnsupportedDate *time.Time `json:"unsupported_date,omitempty" yaml:"unsupported_date,omitempty"`
}

// DeviceName represents a Unique name for a given devices
type DeviceName struct {
	Name       string     `json:"name,omitempty" yaml:"name,omitempty"`
	LastActive *time.Time `json:"last_active,omitempty" yaml:"last_active,omitempty"`
}

// DeviceNames is a list of DeviceName items
type DeviceNames []DeviceName

// MarshalCSV tells the csvutil how to marshal this
func (n DeviceNames) MarshalCSV() ([]byte, error) {
	r := strings.Builder{}
	for _, item := range n {
		r.WriteString(item.Name)
		r.WriteString(" ")
	}
	rs := strings.TrimSpace(r.String())
	return []byte(rs), nil
}

// SSID is an...SSID, I dunno
type SSID struct {
	SSID       string     `json:"ssid,omitempty" yaml:"ssid,omitempty"`
	LastActive *time.Time `json:"last_active,omitempty" yaml:"last_active,omitempty"`
}

// SSIDs represents multiple SSID items
type SSIDs []SSID

// QuarantineRisk describes why a device is at risk for Quarantining
type QuarantineRisk struct {
	Description          string `json:"description,omitempty"`
	NotifyOnly           bool   `json:"notify_only,omitempty"`
	TargetQuarantineDate string `json:"target_quarantine_date,omitempty"`
	Type                 string `json:"type,omitempty"`
}

// QuarantineRisks is a list of QuarantineRisk items
type QuarantineRisks []QuarantineRisk

// MarshalCSV marshals this when called to a csv
func (q QuarantineRisks) MarshalCSV() ([]byte, error) {
	if len(q) > 0 {
		return []byte("Yes"), nil
	}
	return []byte("No"), nil
}

// QuarantineReason represents a reason why a host is under Quarantine
type QuarantineReason struct {
	ID    float64 `json:"id,omitempty"`
	Notes string  `json:"notes,omitempty"`
	Type  string  `json:"type,omitempty"`
}

// QuarantineReasons is multiple QuarantineReason items
type QuarantineReasons []QuarantineReason

// Device represents all that Planisphere knows about a given device
type Device struct {
	LastActive        *time.Time     `json:"last_active,omitempty" yaml:"last_active,omitempty" csv:"last_active,omitempty"`
	Names             DeviceNames    `json:"names,omitempty" yaml:"names,omitempty" csv:"last_active,omitempty"`
	PrimaryName       string         `json:"primary_name,omitempty" yaml:"primary_name,omitempty" csv:"last_active,omitempty"`
	Hostname          Strings        `json:"hostname" yaml:"hostname" csv:"hostname"`
	CustomFields      map[string]any `json:"custom_fields,omitempty" yaml:"custom_fields,omitempty" csv:"-"`
	CrowdstrikeStatus string         `json:"crowdstrike_status" yaml:"crowdstrike_status" csv:"crowdstrike_status"`
	DataSources       []DataSource   `json:"data_sources,omitempty" yaml:"data_sources,omitempty" csv:"-"`
	DepartmentKey     string         `json:"department_key,omitempty" yaml:"department_key,omitempty" csv:"department_key,omitempty"`
	DeviceType        string         `json:"device_type,omitempty" yaml:"device_type,omitempty" csv:"last_active,omitempty"`
	// DiskEncrypted []bool ... Not sure if we should include this...need to see how it looks in a bigger sample
	EndpointMgmtStatus string     `json:"endpoint_mgmt_status,omitempty" yaml:"endpoint_mgmt_status,omitempty" csv:"-"`
	HasUnsupportedOS   bool       `json:"has_unsupported_os" yaml:"has_unsupported_os" csv:"has_unsupported_os"`
	ID                 int64      `json:"id,omitempty" yaml:"id,omitempty" csv:"last_active,omitempty"`
	InEndpointMgmt     bool       `json:"in_endpoint_mgmt,omitempty" csv:"-"`
	LastInEndpointMgmt *time.Time `json:"last_in_endpoint_mgmt,omitempty" yaml:"last_in_endpoint_mgmt,omitempty" csv:",omitempty"`
	Legacy             bool       `json:"legacy" yaml:"legacy" csv:"legacy"`
	MacAddresses       []struct {
		Address    string     `json:"address,omitempty" yaml:"address,omitempty"`
		LastActive *time.Time `json:"last_active,omitempty" yaml:"last_active,omitempty"`
	} `json:"mac_addresses,omitempty" yaml:"mac_addresses,omitempty" csv:"-"`
	Manufacturer         string            `json:"manufacturer,omitempty" yaml:"manufacturer,omitempty" csv:"last_active,omitempty"`
	MemoryMb             float64           `json:"memory_mb,omitempty" yaml:"memory_mb,omitempty" csv:"last_active,omitempty"`
	Model                string            `json:"model,omitempty" yaml:"model,omitempty" csv:"last_active,omitempty"`
	Notes                string            `json:"notes,omitempty" yaml:"notes,omitempty" csv:"notes"`
	Oses                 []Os              `json:"oses,omitempty" yaml:"oses,omitempty" csv:"-"`
	OSFamily             Strings           `json:"os_family" yaml:"os_family" csv:"os_family"`
	OSName               Strings           `json:"os_name" yaml:"os_name" csv:"os_name"`
	OSFullName           Strings           `json:"os_fullname" yaml:"os_fullname" csv:"os_fullname"`
	OSStatus             string            `json:"os_status" yaml:"os_status" csv:"os_status"`
	OSUnsupportedDates   ShortTimestamps   `json:"os_unsupported_dates,omitempty" yaml:"os_unsupported_dates,omitempty" csv:"os_unsupported_dates,omitempty"`
	Owner                string            `json:"owner,omitempty" yaml:"owner,omitempty" csv:"owner,omitempty"`
	PurchaseDate         string            `json:"purchase_date,omitempty" yaml:"purchase_date,omitempty" csv:"purchase_date,omitempty"`
	QuarantineReasons    QuarantineReasons `json:"quarantine_reasons,omitempty" yaml:"quarantine_reasons,omitempty" csv:"-"`
	QuarantineRisks      QuarantineRisks   `json:"quarantine_risks,omitempty" yaml:"quarantine_risks,omitempty" csv:"quarantine_risks,omitempty"`
	Quarantined          bool              `json:"quarantined" yaml:"quarantined" csv:"quarantined"`
	RequiresEndpointMgmt bool              `json:"requires_endpoint_mgmt" yaml:"requires_endpoint_mgmt" csv:"requires_endpoint_management,omitempty"`
	Serial               string            `json:"serial,omitempty" yaml:"serial,omitempty" csv:"serial,omitempty"`
	SecurityScore        float64           `json:"security_score" yaml:"security_score" csv:"security_score,omitempty"`
	SSIDs                SSIDs             `json:"ssids,omitempty" yaml:"ssids,omitempty" csv:"-"`
	Status               string            `json:"status,omitempty" yaml:"status,omitempty" csv:"status,omitempty"`
	StaticIPs            StaticIPs         `json:"static_ips" yaml:"static_ips" csv:"static_ips"`
	Subnets              *DeviceSubnets    `json:"subnets,omitempty" yaml:"subnets,omitempty" csv:"-"`
	SupportGroup         SupportGroup      `json:"support_group,omitempty" yaml:"support_group,omitempty"`
	SupportGroupID       float64           `json:"support_group_id,omitempty" yaml:"support_group_id,omitempty" csv:"owner,omitempty"`
	SupportStaff         Strings           `json:"support_staff,omitempty" yaml:"support_staff,omitempty" csv:"-"`
	URL                  string            `json:"url,omitempty" yaml:"url,omitempty" csv:"owner,omitempty"`
	UsageType            string            `json:"usage_type,omitempty" yaml:"user_type,omitempty" csv:"-"`
	User                 User              `json:"user,omitempty" yaml:"user,omitempty" csv:"-"`
	UserID               int               `json:"user_id,omitempty" yaml:"user_id,omitempty" csv:"-"`
	Vrfs                 []struct {
		LastActive *time.Time `json:"last_active,omitempty" yaml:"last_active,omitempty"`
		Vrf        string     `json:"vrf,omitempty" yaml:"vrf,omitempty"`
	} `json:"vrfs,omitempty" yaml:"vrfs,omitempty" csv:"-"`
	// CollectedVulnerabilities is me hacking the Vulnerabilities endpoint and shoving it in to this field, pretending it's native
	// CollectedVulnerabilities  *VulnerabilityList   `json:"collected_vulnerabilities,omitempty" yaml:"collected_vulnerabilities,omitempty" csv:"-"`
	VulnerabilityDetections   *VulnerabilityList   `json:"vulnerability_detections,omitempty" yaml:"vulnerability_detections,omitempty" csv:"vulnerability_detections,omitempty"`
	Vulnerabilities           *VulnerabilityShorts `json:"vulnerabilities,omitempty" yaml:"vulnerabilities,omitempty" csv:"vulnerabilities,omitempty"`
	VulnerabilitiesActionable *VulnerabilityShorts `json:"vulnerabilities_actionable,omitempty" yaml:"vulnerabilities_actionable,omitempty" csv:"vulnerabilities_actionable,omitempty"`
	VulnerabilitiesIgnorable  *VulnerabilityShorts `json:"vulnerabilities_informational,omitempty" yaml:"vulnerabilities_informational,omitempty" csv:"vulnerabilities_informational,omitempty"`
	WarrantyExpirationDate    *ShortTimestamp      `json:"warranty_expiration_date,omitempty" yaml:"warranty_expiration_date,omitempty" csv:"warranty_expiration_date,omitempty"`
}

// DeviceSummary is a subset of Device data, useful for briefly describing a
// device in various places
type DeviceSummary struct {
	Name string `csv:"name"`
	URL  string `csv:"url"`
}

// DeviceSubnet is the Subnet object returned in a device
type DeviceSubnet struct {
	GuestNetwork bool       `json:"guest_network,omitempty" yaml:"guest_network" csv:"guest_network"`
	LastActive   *time.Time `json:"last_active,omitempty" yaml:"last_active,omitempty" csv:"last_active,omitempty"`
	Name         string     `json:"name,omitempty" yaml:"name,omitempty" csv:"name"`
	Subnet       string     `json:"subnet,omitempty" yaml:"subnet,omitempty" csv:"subnet"`
}

// DeviceSubnets is a list of DeviceSubnet items
type DeviceSubnets []DeviceSubnet

// MarshalCSV makes the csv output correct
func (d DeviceSummary) MarshalCSV() ([]byte, error) {
	return []byte(fmt.Sprintf("%v (%v)", d.Name, d.URL)), nil
	// return strconv.AppendInt(nil, int64(f), 16), nil
}

// Summary returns a DeviceSummary from a given Device
func (d Device) Summary() DeviceSummary {
	return DeviceSummary{
		Name: d.PrimaryName,
		URL:  d.URL,
	}
}

// User is a user my friend
type User struct {
	Affiliation   string `json:"affiliation,omitempty"`
	DepartmentKey string `json:"department_key,omitempty"`
	DisplayName   string `json:"display_name,omitempty"`
	ID            int    `json:"id,omitempty"`
	Netid         string `json:"netid,omitempty"`
}

// ShortTimestamp is just the date in yyyy-mm-dd format
type ShortTimestamp struct {
	time.Time
}

// ShortTimestamps is a collection of ShortTimestamp objects
type ShortTimestamps []ShortTimestamp

// MarshalCSV tells the csvutil how to marshal this
func (n ShortTimestamps) MarshalCSV() ([]byte, error) {
	ss := make([]string, len(n))
	for idx, item := range n {
		ss[idx] = fmt.Sprint(item)
	}
	return []byte(strings.Join(ss, ", ")), nil
}

// UnmarshalJSON tells the json library how to handle this as a unix timestamp
func (t *ShortTimestamp) UnmarshalJSON(s []byte) error {
	r := string(s)
	// If set to null string, just return
	if r == `null` {
		return nil
	}
	r = strings.TrimLeft(strings.TrimRight(r, `"`), `"`)
	var err error
	tt, err := time.Parse("2006-01-02", r)
	if err != nil {
		return err
	}
	t.Time = tt
	return nil
}

// StaticIP is the static ip object for a given device
type StaticIP struct {
	LastActive *time.Time `json:"last_active" yaml:"last_active" csv:"last_active"`
	Canonical  string     `json:"canonical" yaml:"canonical" csv:"canonical"`
}

// StaticIPs is a collections of StaticIP objects
type StaticIPs []StaticIP

// MarshalCSV tells the csvutil how to marshal this
func (n StaticIPs) MarshalCSV() ([]byte, error) {
	ss := make([]string, len(n))
	for idx, item := range n {
		ss[idx] = fmt.Sprint(item.Canonical)
	}
	return []byte(strings.Join(ss, ", ")), nil
}

// Strings is simple type for multiple strings
type Strings []string

// MarshalCSV tells the csvutil how to marshal this
func (n Strings) MarshalCSV() ([]byte, error) {
	return []byte(strings.Join(n, ", ")), nil
}
