/*
Package planisphere implents a client for interacting with the Planisphere API
*/
package planisphere

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"os"

	"moul.io/http2curl/v2"
)

const (
	// libraryVersion = "0.0.1"
	userAgent = "planisphere-sdk"
	// mediaType = "application/json"
	baseURL = "https://planisphere.oit.duke.edu"

	maxConnections = 10
)

// Client is the beefy part of this SDK. It provides the methods used to interact with the API
type Client struct {
	// HTTP client used to communicate with the DO API.
	client *http.Client

	// Token for requests
	Username string
	Token    string

	// Base URL for API requests.
	BaseURL *url.URL

	// User agent for client
	UserAgent string

	// printCurl
	printCurl bool

	supportGroupScope   SupportGroupScope
	scopedSupportGroups *SupportGroupList

	// Optional function called after every successful request made to the DO APIs
	// onRequestCompleted RequestCompletionCallback

	// Optional extra HTTP headers to set on every request to the API.
	// headers map[string]string

	Department     DepartmentService
	SupportGroup   SupportGroupService
	Device         DeviceService
	Subnet         SubnetService
	Report         ReportService
	ScanExemptions ScanExemptionsService
	ScanPolicy     ScanPolicyService
}

// type RequestCompletionCallback func(*http.Request, *http.Response)

// NewClient generates a new Client object with the given username and password
// Deprecated: Use New() instead with functional options
func NewClient(username string, token string, httpClient *http.Client) *Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	baseURL, _ := url.Parse(baseURL)
	c := &Client{client: httpClient, BaseURL: baseURL, UserAgent: userAgent, Username: username, Token: token}

	// See if PRINT_CURL is set and save it, so we only have to do this lookup once
	if os.Getenv("PRINT_CURL") != "" {
		c.printCurl = true
	}

	c.Department = &DepartmentServiceOp{client: c}
	c.SupportGroup = &SupportGroupServiceOp{client: c}
	c.Device = &DeviceServiceOp{client: c}
	c.Subnet = &SubnetServiceOp{client: c}
	c.Report = &ReportServiceOp{client: c}
	c.ScanExemptions = &ScanExemptionsServiceOp{client: c}
	c.ScanPolicy = &ScanPolicyServiceOp{client: c}

	return c
}

// MustNew returns a new client object, or panics
func MustNew(options ...func(*Client)) *Client {
	c, err := New(options...)
	if err != nil {
		panic(err)
	}
	return c
}

// New uses functional arguments to generate a new client
func New(options ...func(*Client)) (*Client, error) {
	baseURL, err := url.Parse(baseURL)
	if err != nil {
		return nil, err
	}
	c := &Client{
		client:  http.DefaultClient,
		BaseURL: baseURL,
	}

	for _, o := range options {
		o(c)
	}

	if c.Username == "" {
		return nil, err
	}
	if c.Token == "" {
		return nil, err
	}

	// See if PRINT_CURL is set and save it, so we only have to do this lookup once
	if os.Getenv("PRINT_CURL") != "" {
		c.printCurl = true
	}

	c.Department = &DepartmentServiceOp{client: c}
	c.SupportGroup = &SupportGroupServiceOp{client: c}
	c.Device = &DeviceServiceOp{client: c}
	c.Subnet = &SubnetServiceOp{client: c}
	c.Report = &ReportServiceOp{client: c}
	c.ScanExemptions = &ScanExemptionsServiceOp{client: c}
	c.ScanPolicy = &ScanPolicyServiceOp{client: c}

	return c, nil
}

// WithScopedSupportGroups sets the scoped support groups
func WithScopedSupportGroups(sgs SupportGroupList) func(*Client) {
	return func(c *Client) {
		c.scopedSupportGroups = &sgs
	}
}

// WithUsername sets the username for authentication
func WithUsername(u string) func(*Client) {
	return func(c *Client) {
		c.Username = u
	}
}

// WithToken sets the token for authentication
func WithToken(t string) func(*Client) {
	return func(c *Client) {
		c.Token = t
	}
}

// WithHTTPClient sets the http.Client for the planisphere client
func WithHTTPClient(hc *http.Client) func(*Client) {
	return func(c *Client) {
		c.client = hc
	}
}

// WithBaseURL sets the base url of the planisphere url
func WithBaseURL(b string) func(*Client) {
	baseURL, err := url.Parse(b)
	if err != nil {
		panic(err)
	}
	return func(c *Client) {
		c.BaseURL = baseURL
	}
}

// WithSupportGroupScope sets the support group strategy for the client
func WithSupportGroupScope(s SupportGroupScope) func(*Client) {
	return func(c *Client) {
		c.supportGroupScope = s
	}
}

// WithEnvAuth sets the username and password using environment variables
func WithEnvAuth() func(*Client) {
	var username string
	for _, item := range []string{"PLANISPHERE_USERNAME", "PLANISPHERE_USER"} {
		u := os.Getenv(item)
		if u != "" {
			username = u
			break
		}
	}
	if username == "" {
		panic("no username found in env in either PLANISPHERE_USERNAME or PLANISPHERE_USER")
	}

	token := os.Getenv("PLANISPHERE_TOKEN")
	if token == "" {
		panic("no token found in env PLANISPHERE_TOKEN")
	}

	return func(c *Client) {
		c.Username = username
		c.Token = token
	}
}

func (c *Client) sendRequest(req *http.Request, v interface{}) (*http.Response, error) {
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "application/json; charset=utf-8")
	req.Header.Set("User-Agent", userAgent)
	req.SetBasicAuth(c.Username, c.Token)

	if c.printCurl {
		command, _ := http2curl.GetCurlCommand(req)
		fmt.Fprintf(os.Stderr, "%v\n", command)
	}

	res, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}

	defer dclose(res.Body)

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		var errRes errorResponse
		if err = json.NewDecoder(res.Body).Decode(&errRes); err == nil {
			return nil, errors.New(errRes.Message)
		}

		if res.StatusCode == http.StatusTooManyRequests {
			return nil, fmt.Errorf("too many requests.  Check rate limit and make sure the userAgent is set right")
		}
		return nil, fmt.Errorf("unknown error, status code: %d", res.StatusCode)
	}

	if err = json.NewDecoder(res.Body).Decode(&v); err != nil {
		return nil, err
	}

	return res, nil
}

// AddScopedSupportGroup will add a support group to the relevant list
func (c *Client) AddScopedSupportGroup(sg SupportGroup) {
	var current *SupportGroupList
	if c.scopedSupportGroups != nil {
		current = c.scopedSupportGroups
	} else {
		current = &SupportGroupList{}
	}
	(*current) = append(*current, sg)
	c.scopedSupportGroups = current
}

// Response is just a holder for some response data we may want to examine later on
type Response struct {
	Response *http.Response
}

type errorResponse struct {
	Status    string `json:"status"`
	ErrorType string `json:"errorType"`
	Error     string `json:"error"`
	Message   string `json:"message,omitempty"`
}

// SupportGroupScope is a type describing how a client should select support groups for querying
type SupportGroupScope int

const (
	// SGScopeMine tells the client to use the support groups the client user is in
	SGScopeMine SupportGroupScope = iota
	// SGScopeAll tells the client to look at all support groups
	SGScopeAll
	// SGScopeSelected tells the client to use the selected support groups
	SGScopeSelected
)
