package planisphere

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestSubnetsList(t *testing.T) {
	got, _, err := c.Subnet.List(context.TODO())
	require.NoError(t, err)
	require.NotNil(t, got)
}
