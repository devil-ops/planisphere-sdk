package planisphere

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

// VulnerabilityCriticality lists the most severe vulnerability criticality for
// a given host
type VulnerabilityCriticality int

// Actionability is whether or not a vulnerability is actionable
type Actionability int

// Actionable is only Actionable Actionablities
const (
	AnyActionability Actionability = iota
	Actionable
	UnActionable
)

var actionabilityStrMap = map[string]Actionability{
	"any":          AnyActionability,
	"actionable":   Actionable,
	"inactionable": UnActionable,
}

var actionabilityBoolMap = map[Actionability]*bool{
	AnyActionability: nil,
	Actionable:       boolPTR(true),
	UnActionable:     boolPTR(false),
}

// GetActionabilities returns all actionabilities
func GetActionabilities() []Actionability {
	res := []Actionability{}
	for _, item := range actionabilityStrMap {
		res = append(res, item)
	}
	return res
}

// ReqBool return the pointer to the bool that should be used in the request
func (a Actionability) ReqBool() *bool {
	return actionabilityBoolMap[a]
}

const (
	// VulnUnset means we don't have any data on the Criticality yet
	VulnUnset VulnerabilityCriticality = 0
	// VulnCritical is the highest criticality
	VulnCritical VulnerabilityCriticality = 50
	// VulnHigh isn't as bad as critical, but still pretty bad
	VulnHigh VulnerabilityCriticality = 40
	// VulnMedium still matters a lil, but we don't consider it to be something worth ticketing in general
	VulnMedium VulnerabilityCriticality = 30
	// VulnNone means no vulnerabilities found
	VulnNone VulnerabilityCriticality = -1
)

var criticalityMap = map[string]VulnerabilityCriticality{
	"":         VulnUnset,
	"critical": VulnCritical,
	"high":     VulnHigh,
	"medium":   VulnMedium,
	"none":     VulnNone,
}

// VulnerabilityCriticalities is multiple VulnerabilityCriticality items
type VulnerabilityCriticalities []VulnerabilityCriticality

// Vulnerability is a single instance of a found Vulnerability on a device
type Vulnerability struct {
	Description        string         `json:"description,omitempty" yaml:"description,omitempty" csv:"description,omitempty"`
	Severity           string         `json:"severity,omitempty" yaml:"severity,omitempty" csv:"severity,omitempty"`
	Details            string         `json:"details,omitempty" yaml:"details,omitempty" csv:"details,omitempty"`
	Solution           string         `json:"solution,omitempty" yaml:"solution,omitempty" csv:"solution,omitempty"`
	FirstSeen          *time.Time     `json:"first_seen,omitempty" yaml:"first_seen,omitempty" csv:"first_seen,omitempty"`
	LastSeen           *time.Time     `json:"last_seen,omitempty" yaml:"last_seen,omitempty" csv:"last_seen,omitempty"`
	ExploitAvailable   bool           `json:"exploit_available,omitempty" yaml:"exploit_available" csv:"exploit_available"`
	Actionable         bool           `json:"actionable" yaml:"actionable" csv:"actionable"`
	AcceptedRisk       bool           `json:"accepted_risk" yaml:"accepted_risk" csv:"accepted_risk"`
	ResponsibilityType string         `json:"responsibility_type" yaml:"responsibility_type" csv:"responsibility_type"`
	IP                 string         `json:"ip,omitempty" yaml:"ip,omitempty" csv:"ip,omitempty"`
	Port               int            `json:"port,omitempty" yaml:"port,omitempty" csv:"port,omitempty"`
	Protocol           string         `json:"protocol,omitempty" yaml:"protocol,omitempty" csv:"protocol,omitempty"`
	ExtraData          map[string]any `json:"extra_data,omitempty" yaml:"extra_data,omitempty" csv:"-"`
}

/*
type Vulnerability struct {
	Current            bool              `json:"current,omitempty" yaml:"current" csv:"current"`
	Data               VulnerabilityData `json:"data,omitempty" yaml:"data,omitempty" csv:"data_,inline"`
	DataSourceID       float64           `json:"data_source_id,omitempty" yaml:"data_source_id,omitempty" csv:"-"`
	DataSourceName     string            `json:"data_source_name,omitempty" yaml:"data_source_name,omitempty" csv:"data_source_name,omitempty"`
	DataTimestamp      *time.Time        `json:"data_timestamp,omitempty" yaml:"data_timestamp,omitempty" csv:"timestamp,omitempty"`
	ExtraData          map[string]any    `json:"extra_data,omitempty" yaml:"extra_data,omitempty" csv:"-"`
	Key                string            `json:"key,omitempty" yaml:"key,omitempty" csv:"key,omitempty"`
	LastActive         *time.Time        `json:"last_active,omitempty" yaml:"last_active,omitempty" csv:"last_active,omitempty"`
	LastUpdated        *time.Time        `json:"last_updated,omitempty" yaml:"last_updated,omitempty" csv:"last_updated,omitempty"`
	URL                string            `json:"url,omitempty" yaml:"url,omitempty" csv:"url,omitempty"`
	Actionable         bool              `json:"actionable" yaml:"actionable" csv:"actionable"`
	AcceptedRisk       bool              `json:"accepted_risk" yaml:"accepted_risk" csv:"accepted_risk"`
	ResponsibilityType string            `json:"responsibility_type" yaml:"responsibility_type" csv:"responsibility_type"`
}
*/

// VulnerabilityData is the juicy data from a given Vulnerability
type VulnerabilityData struct {
	Description           string                      `json:"description,omitempty" yaml:"description,omitempty" csv:"description,omitempty"`
	ExploitAvailable      bool                        `json:"exploit_available,omitempty" yaml:"exploit_available" csv:"exploit_available"`
	FirstObserved         *VulnerabilityDataTimestamp `json:"first_observed,omitempty" yaml:"first_observed,omitempty" csv:"first_observed,omitempty"`
	PendingNextPatchset   bool                        `json:"pending_next_patchset" yaml:"pending_next_patchset" csv:"pending_next_patchset"`
	IP                    string                      `json:"ip,omitempty" yaml:"ip,omitempty" csv:"ip,omitempty"`
	Port                  string                      `json:"port,omitempty" yaml:"port,omitempty" csv:"port,omitempty"`
	Protocol              string                      `json:"protocol,omitempty" yaml:"protocol,omitempty" csv:"protocol,omitempty"`
	Solution              string                      `json:"solution,omitempty" yaml:"solution,omitempty" csv:"solution,omitempty"`
	VulnerabilityDetails  string                      `json:"vulnerability_details,omitempty" yaml:"vulnerability_details,omitempty" csv:"vulnerability_details,omitempty"`
	VulnerabilitySeverity string                      `json:"vulnerability_severity,omitempty" yaml:"vulnerability_severity,omitempty" csv:"vulnerability_severity,omitempty"`
}

// VulnerabilityDataTimestamp is just the date in unixtime, it'll need to be unmarshalled specially though
type VulnerabilityDataTimestamp struct {
	time.Time
}

// UnmarshalJSON tells the json library how to handle this as a unix timestamp
func (t *VulnerabilityDataTimestamp) UnmarshalJSON(s []byte) error {
	r := string(s)
	q, err := strconv.ParseInt(r, 10, 64)
	if err != nil {
		return err
	}
	t.Time = time.Unix(q, 0)
	return nil
}

// VulnerabilityList is multiple vulnerabilities
type VulnerabilityList []*Vulnerability

// VulnerabilityCriticalityNames Return a list of criticality names.
func VulnerabilityCriticalityNames() []string {
	// Length of the map, minus the empty one
	keys := make([]string, len(criticalityMap)-1)

	i := 0
	for k := range criticalityMap {
		if k != "" {
			keys[i] = k
			i++
		}
	}
	return keys
}

// GetVulnerabilityCriticality returns the criticality value from the name
func GetVulnerabilityCriticality(name string) (VulnerabilityCriticality, error) {
	critValue, ok := criticalityMap[strings.ToLower(name)]
	if !ok {
		return VulnUnset, fmt.Errorf("invalid criticality: %s", name)
	}

	return critValue, nil
}

// VulnerabilityShort is a short representation of a vulnerability that shows up in the Device report
type VulnerabilityShort struct {
	Description string `json:"description" yaml:"description" csv:"description"`
	Severity    string `json:"severity" yaml:"severity" csv:"severity"`
}

// VulnerabilityShorts is multiple VulnerabilityShort objects
type VulnerabilityShorts []VulnerabilityShort

// MarshalCSV tells the csvutil how to marshal this
func (n VulnerabilityShorts) MarshalCSV() ([]byte, error) {
	ss := make([]string, len(n))
	for idx, item := range n {
		ss[idx] = fmt.Sprintf("%v (%v)", item.Description, item.Severity)
	}
	return []byte(strings.Join(ss, "\n")), nil
}

// VulnFilterParams describes how to filter the vulns
type VulnFilterParams struct {
	Actionability  Actionability
	Responsibility Responsibility
	Risk           Risk
}

// VulnFilter filters vulnerabilities
type VulnFilter func(VulnFilterParams, Vulnerability) bool

// VulnFilterResponsibility filters on the responsible party
func VulnFilterResponsibility(p VulnFilterParams, v Vulnerability) bool {
	if p.Responsibility == AnyResponsibility {
		return true
	}
	return p.Responsibility == Responsibility(v.ResponsibilityType)
}

// VulnFilterActionability filters based off if if the vulnerability is actionable
func VulnFilterActionability(p VulnFilterParams, v Vulnerability) bool {
	switch p.Actionability {
	case AnyActionability:
		return true
	case Actionable:
		return v.Actionable
	case UnActionable:
		return !v.Actionable
	default:
		panic("unexpected actionability")
	}
}

// VulnFilterRisk filters based off of risk
func VulnFilterRisk(p VulnFilterParams, v Vulnerability) bool {
	switch p.Risk {
	case AnyRisk:
		return true
	case AcceptedRisk:
		return v.AcceptedRisk
	case UnAcceptedRisk:
		return !v.AcceptedRisk
	default:
		panic("unexpected risk")
	}
}

// ApplyAllVulnFilters is a helper wrapper to check all the filters
func ApplyAllVulnFilters(params VulnFilterParams, records VulnerabilityList) VulnerabilityList {
	return applyVulnFilters(
		params,
		records,
		VulnFilterActionability,
		VulnFilterResponsibility,
		VulnFilterRisk,
	)
}

// applyVulnFilters filters out vulnerabilities from a list
func applyVulnFilters(params VulnFilterParams, records VulnerabilityList, filters ...VulnFilter) VulnerabilityList {
	if len(filters) == 0 {
		return records
	}

	filteredRecords := make(VulnerabilityList, 0, len(records))

	// Range over the records and apply all the filters to each record.
	// If the record passes all the filters, add it to the final slice.
	for _, r := range records {
		keep := true

		for _, f := range filters {
			if !f(params, *r) {
				keep = false
				break
			}
		}

		if keep {
			filteredRecords = append(filteredRecords, r)
		}
	}

	return filteredRecords
}

// Responsibility is a type for who is responsible for a given vuln
type Responsibility string

var (
	// AnyResponsibility brings all responsibilities in
	AnyResponsibility Responsibility = "any"
	// OSResponsibility lies on the OS admins
	OSResponsibility Responsibility = "os"
	// AppResponsibility lies on the App admins
	AppResponsibility Responsibility = "app"
)

// String is used both by fmt.Print and by Cobra in help text
func (r *Responsibility) String() string {
	return string(*r)
}

// Risk is a type for risk acceptance
type Risk int

const (
	// AnyRisk is either accepted or unaccepted
	AnyRisk Risk = iota
	// AcceptedRisk is accepted
	AcceptedRisk
	// UnAcceptedRisk is not accepted
	UnAcceptedRisk
)

var riskMap = map[Risk]*bool{
	AnyRisk:        nil,
	AcceptedRisk:   boolPTR(true),
	UnAcceptedRisk: boolPTR(false),
}

// ReqBool return the pointer to the bool that should be used in the request
func (r Risk) ReqBool() *bool {
	return riskMap[r]
}
