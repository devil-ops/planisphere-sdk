package planisphere

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestExemptIPs(t *testing.T) {
	require.NotNil(t, c.ScanExemptions)
	got, _, err := c.ScanExemptions.StaticIPs(context.Background())
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Equal(t, &ExemptStaticIPs{"1.1.1.1", "10.0.0.1", "fce7:::::1"}, got)
}

func TestExemptSubnets(t *testing.T) {
	require.NotNil(t, c.ScanExemptions)
	got, _, err := c.ScanExemptions.Subnets(context.Background())
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Equal(t, &ExemptSubnets{"10.0.0.0/24", "fce7:::::0/126"}, got)
}

func TestExemptWirelessSubnets(t *testing.T) {
	require.NotNil(t, c.ScanExemptions)
	got, _, err := c.ScanExemptions.WirelessSubnets(context.Background())
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Equal(t, &ExemptWirelessSubnets{"192.168.1.0/24", "faa7:::::0/127"}, got)
}

func TestExemptAll(t *testing.T) {
	require.NotNil(t, c.ScanExemptions)
	got, _, err := c.ScanExemptions.All(context.Background())
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Equal(
		t,
		&AllExemptions{
			StaticIPs:       ExemptStaticIPs{"1.1.1.1", "10.0.0.1", "fce7:::::1"},
			Subnets:         ExemptSubnets{"10.0.0.0/24", "fce7:::::0/126"},
			WirelessSubnets: ExemptWirelessSubnets{"192.168.1.0/24", "faa7:::::0/127"},
		},
		got,
	)

	require.Equal(
		t,
		[]string{"1.1.1.1", "10.0.0.0/24", "10.0.0.1", "192.168.1.0/24", "faa7:::::0/127", "fce7:::::0/126", "fce7:::::1"},
		got.All(),
	)
}
