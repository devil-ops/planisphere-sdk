package planisphere

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestSupportGroupList(t *testing.T) {
	got, _, err := c.SupportGroup.List(context.TODO())
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestSupportGroupKeysWithUser(t *testing.T) {
	got, _, err := c.SupportGroup.KeysWithUser(context.TODO(), "mary")
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Equal(t, []string{"example:groups:foo"}, got)
}

func TestSupportGroupIDsWithUser(t *testing.T) {
	got, _, err := c.SupportGroup.IDsWithUser(context.TODO(), "mary")
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Equal(t, []float64{356}, got)
}
