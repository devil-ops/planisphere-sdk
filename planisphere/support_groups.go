package planisphere

import (
	"context"
	"errors"
	"fmt"
	"strings"
)

// SupportGroup represents information about the group that supports a given device
type SupportGroup struct {
	ID                    float64 `json:"id,omitempty" yaml:"id"`
	Key                   string  `json:"key,omitempty" yaml:"key,omitempty"`
	Name                  string  `json:"name,omitempty" yaml:"name,omitempty"`
	ServiceNowDisplayName string  `json:"service_now_display_name,omitempty" yaml:"service_now_display_name,omitempty"`
	ServiceNowSysID       string  `json:"service_now_sys_id,omitempty" yaml:"service_now_sys_id,omitempty"`
	UserNetids            *NetIDs `json:"user_netids,omitempty" yaml:"user_netids,omitempty"`
}

// NetID represents a unique user
type NetID string

// NetIDs represents multiple NetID items
type NetIDs []NetID

// Contains returns true if a NetID is the member of a NetIDs
func (ns NetIDs) Contains(u NetID) bool {
	for i := 0; i < len(ns); i++ {
		if ns[i] == u {
			return true
		}
	}
	return false
}

// MarshalCSV handles marshaling for CSV encoding
func (ns NetIDs) MarshalCSV() ([]byte, error) {
	items := make([]string, len(ns))
	for idx, i := range ns {
		items[idx] = fmt.Sprint(i)
	}
	return []byte(strings.Join(items, ", ")), nil
}

// MarshalCSV handles marshaling for CSV encoding
func (s SupportGroup) MarshalCSV() ([]byte, error) {
	return []byte(s.Key), nil
}

// SupportGroupList contains multiple SupportGroup items
type SupportGroupList []SupportGroup

// IDs returns only the ids of the support group list
func (s SupportGroupList) IDs() []float64 {
	ret := make([]float64, len(s))
	for idx, item := range s {
		ret[idx] = item.ID
	}
	return ret
}

const (
	supportGroupPath = "/api/v1/support_groups"
)

// SupportGroupService is the interface providing methods for the support_group endpoint
type SupportGroupService interface {
	List(context.Context) (*SupportGroupList, *Response, error)
	Get(context.Context, string) (*SupportGroup, *Response, error)
	Mine(context.Context) (*SupportGroupList, *Response, error)
	KeysWithUser(context.Context, string) ([]string, *Response, error)
	IDsWithUser(context.Context, string) ([]float64, *Response, error)
}

// SupportGroupServiceOp is the Operator for the SupportGroupService endpoint
type SupportGroupServiceOp struct {
	client *Client
}

// List lists out all SupportGroups
func (svc *SupportGroupServiceOp) List(_ context.Context) (*SupportGroupList, *Response, error) {
	req := mustNewGetRequest(fmt.Sprintf("%s%s", svc.client.BaseURL, supportGroupPath))
	r := &SupportGroupList{}
	resp := &Response{}
	var err error

	resp.Response, err = svc.client.sendRequest(req, &r) // nolint:golint,bodyclose
	if err != nil {
		return nil, resp, err
	}
	dclose(resp.Response.Body)
	return r, resp, nil
}

// Mine retrieves support groups that the authenticated user is a member of
func (svc *SupportGroupServiceOp) Mine(ctx context.Context) (*SupportGroupList, *Response, error) {
	allGroups, resp, err := svc.List(ctx)
	if err != nil {
		return nil, resp, err
	}
	defer dclose(resp.Response.Body)
	var filteredGroups SupportGroupList
	for _, g := range *allGroups {
		// if containsString(g.UserNetids, svc.client.Username) {
		if g.UserNetids.Contains(NetID(svc.client.Username)) {
			// if itemExists(g.UserNetids, svc.client.Username) {
			filteredGroups = append(filteredGroups, g)
		}
	}
	return &filteredGroups, resp, nil
}

// Get retrieves support groups with a given 'key', 'name' or 'id' (In that order)
func (svc *SupportGroupServiceOp) Get(ctx context.Context, key string) (*SupportGroup, *Response, error) {
	allGroups, resp, err := svc.List(ctx)
	if err != nil {
		return nil, resp, err
	}
	defer dclose(resp.Response.Body)
	// Check keys
	for _, g := range *allGroups {
		if g.Key == key {
			return &g, resp, nil
		}
	}
	// Check names
	for _, g := range *allGroups {
		if strings.EqualFold(g.Name, key) {
			return &g, resp, nil
		}
	}

	// Check IDs
	for _, g := range *allGroups {
		if fmt.Sprint(g.ID) == key {
			return &g, resp, nil
		}
	}
	return nil, resp, errors.New("no group with that key found")
}

// KeysWithUser retrieves support group keys using a username
func (svc *SupportGroupServiceOp) KeysWithUser(ctx context.Context, user string) ([]string, *Response, error) {
	allGroups, resp, err := svc.List(ctx)
	if err != nil {
		return nil, resp, err
	}
	defer dclose(resp.Response.Body)
	filteredGroups := []string{}
	for _, g := range *allGroups {
		if g.UserNetids.Contains(NetID(user)) {
			filteredGroups = append(filteredGroups, g.Key)
		}
	}
	return filteredGroups, resp, nil
}

// IDsWithUser returns the support group ids when given a user name
func (svc *SupportGroupServiceOp) IDsWithUser(_ context.Context, user string) ([]float64, *Response, error) {
	req := mustNewGetRequest(fmt.Sprintf("%s%s", svc.client.BaseURL, supportGroupPath))
	allGroups := &SupportGroupList{}
	filteredGroups := []float64{}
	var err error
	resp := &Response{}

	resp.Response, err = svc.client.sendRequest(req, &allGroups) // nolint:golint,bodyclose
	if err != nil {
		return nil, resp, err
	}
	for _, g := range *allGroups {
		if g.UserNetids.Contains(NetID(user)) {
			filteredGroups = append(filteredGroups, g.ID)
		}
	}
	dclose(resp.Response.Body)
	return filteredGroups, resp, nil
}
