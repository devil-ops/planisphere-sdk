package planisphere

import (
	"context"
	"fmt"
)

// RecentIPs represents an array of strings for IP addresses that have recently been active
type RecentIPs []string

// Strings converts the recent ips to a string array
func (r RecentIPs) Strings() []string {
	ret := make([]string, len(r))
	l := copy(ret, r)
	if l != len(r) {
		panic("error copying ips to strings")
	}
	return ret
}

// ScanPolicyService describes how the ScanPolicy endpoint works
type ScanPolicyService interface {
	RecentIPs(context.Context, int) (*RecentIPs, *Response, error)
}

// ScanPolicyServiceOp actually implements the ScanPolicyService
type ScanPolicyServiceOp struct {
	client *Client
}

// RecentIPs returns recent IPs for a given scan policy id
func (svc *ScanPolicyServiceOp) RecentIPs(_ context.Context, id int) (*RecentIPs, *Response, error) {
	req := mustNewGetRequest(fmt.Sprintf("%s/api/v1/vulnerability/scan_policies/%v/recent_ips", svc.client.BaseURL, id))
	r := &RecentIPs{}
	resp := &Response{}
	var err error
	resp.Response, err = svc.client.sendRequest(req, &r) //nolint:golint,bodyclose
	if err != nil {
		return nil, resp, err
	}
	dclose(resp.Response.Body)

	return r, resp, nil
}
