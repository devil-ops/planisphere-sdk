package planisphere

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"reflect"
	"strings"
	"time"

	"github.com/google/uuid"
	"moul.io/http2curl/v2"
)

/*
Values documented on planisphere here:
  https://planisphere.oit.duke.edu/help/self_report
*/

// SelfReportPayloadData is the actual payload for the SelfReport endpoint
type SelfReportPayloadData struct {
	OsFullname            string                `json:"os_fullname,omitempty"`
	OsFamily              string                `json:"os_family,omitempty"`
	Serial                string                `json:"serial,omitempty"`
	DepartmentKey         string                `json:"department_key,omitempty"`
	DeviceType            string                `json:"device_type,omitempty"`
	DiskEncrypted         bool                  `json:"disk_encrypted,omitempty"`
	Hostname              string                `json:"hostname,omitempty"`
	InstalledSoftware     [][]string            `json:"installed_software,omitempty"`
	MacAddresses          []string              `json:"mac_addresses,omitempty"`
	Manufacturer          string                `json:"manufacturer,omitempty"`
	MemoryMB              uint64                `json:"memory_mb,omitempty"`
	Model                 string                `json:"model,omitempty"`
	Status                string                `json:"status,omitempty"`
	SupportGroupID        uint64                `json:"support_group_id,omitempty"`
	SupportGroupName      string                `json:"support_group_name,omitempty"`
	UsageType             string                `json:"usage_type,omitempty"`
	Username              string                `json:"username,omitempty"`
	OSExtendedSupport     string                `json:"os_extended_support,omitempty"`
	ExternalOSIdentifiers ExternalOSIdentifiers `json:"external_os_identifiers,omitempty"`
	// ExternalOSIdentifiers [][]string `json:"external_os_identifiers,omitempty"`
}

// ExternalOSIdentifiers is a simple map[string]string for shortcuts to OS IDs
type ExternalOSIdentifiers map[string]string

// UnmarshalJSON is the custom unmarshaller
func (c *ExternalOSIdentifiers) UnmarshalJSON(data []byte) error {
	origResults := [][]string{}
	modResults := map[string]string{}
	err := json.Unmarshal(data, &origResults)
	if err != nil {
		return err
	}
	for _, item := range origResults {
		modResults[item[0]] = item[1]
	}
	*c = modResults
	return nil
}

// SelfReportPayload is the payload data plus some additional meta information
type SelfReportPayload struct {
	LastActive time.Time             `json:"last_active,omitempty"`
	Key        string                `json:"key,omitempty"`
	URL        string                `json:"url,omitempty"`
	Data       SelfReportPayloadData `json:"data,omitempty"`
	ExtraData  map[string]string     `json:"extra_data,omitempty"`
}

// SelfReportTestPayload is what we get back when using the test endpoint
type SelfReportTestPayload struct {
	ProcessedRecord SelfReportPayload `json:"processed_record,omitempty"`
	Status          string            `json:"status,omitempty"`
}

// Validate returns an error if the SelfReportPayload is invalid
func (p SelfReportPayload) Validate() error {
	// Ensure either a serial or mac address list is set
	if p.Data.Serial == "" && len(p.Data.MacAddresses) == 0 {
		return fmt.Errorf("must set either serial or mac_addresses")
	}

	if p.LastActive.IsZero() {
		return fmt.Errorf("must set last_active")
	}

	// Ensure the device_type is valid
	if p.Data.DeviceType != "" {
		validDevices := []string{"desktop", "laptop", "server", "server_physical", "vm"}
		if !containsString(validDevices, p.Data.DeviceType) {
			return fmt.Errorf("%s is not a valid device type", p.Data.DeviceType)
		}
	}
	// Ensure the usage_type is valid
	if p.Data.UsageType != "" {
		validUsages := []string{"assigned_user", "loaner", "self_managed", "server", "shared_use"}
		if !containsString(validUsages, p.Data.UsageType) {
			return fmt.Errorf("%s is not a valid usage type", p.Data.UsageType)
		}
	}
	// Ensure the status is valid
	if p.Data.Status != "" {
		validStatuses := []string{"deployed", "for_parts", "lost", "repair", "rma", "staging", "stored", "surplused"}
		if !containsString(validStatuses, p.Data.Status) {
			return fmt.Errorf("%s is not a valid status", p.Data.Status)
		}
	}
	return nil
}

// Submit pushes the report to the API endpoint
func (p SelfReportPayload) Submit(key string) error {
	if ValidateSelfReportKey(key) != nil {
		return fmt.Errorf("invalid key. Ensure you are using a valid self-report key from the Planisphere: https://duke.is/vywtw")
	}
	c := http.Client{}
	lookupURL, _ := os.LookupEnv("PLANISPHEREREPORT_URL")
	var url string
	if lookupURL == "" {
		url = "https://planisphere.oit.duke.edu/self_report"
	} else {
		url = lookupURL
	}
	// Make sure the payload is valid
	err := p.Validate()
	if err != nil {
		return err
	}

	// Convert to JSON
	payloadJSON, err := json.Marshal(p)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(payloadJSON))
	if err != nil {
		return err
	}

	if os.Getenv("PRINT_CURL") != "" {
		command, _ := http2curl.GetCurlCommand(req)
		fmt.Fprintf(os.Stderr, "%v", command)
	}
	// Add some required headers
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("PLANISPHERE-REPORT-KEY", key)
	req.Header.Add("User-Agent", "planisphere-report-go")
	resp, err := c.Do(req)
	if err != nil {
		return err
	}
	defer dclose(resp.Body)
	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		return nil
	}
	return fmt.Errorf("error making request to post data to planisphere 😢")
}

// ValidateSelfReportKey makes sure the key we are passing is an actual key
func ValidateSelfReportKey(key string) error {
	if strings.ToLower(key) != key {
		return errors.New("key must be completely lowercase")
	}
	if !strings.Contains(key, "-") {
		return errors.New("key does not contain a hyphen")
	}
	_, err := uuid.Parse(key)
	if err != nil {
		return errors.New("key does not match UUID format")
	}
	return nil
}

// SubmitTest sends the payload to the test endpoint
func (p SelfReportPayload) SubmitTest(key string) (*SelfReportTestPayload, error) {
	if err := ValidateSelfReportKey(key); err != nil {
		return nil, err
	}
	c := http.Client{}
	lookupURL, _ := os.LookupEnv("PLANISPHEREREPORT_URL")
	var url string
	if lookupURL == "" {
		url = "https://planisphere.oit.duke.edu/self_report/test"
	} else {
		url = lookupURL
	}
	// Make sure the payload is valid
	err := p.Validate()
	if err != nil {
		return nil, err
	}

	// Convert to JSON
	payloadJSON, err := json.Marshal(p)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(payloadJSON))
	if err != nil {
		return nil, err
	}
	command, _ := http2curl.GetCurlCommand(req)
	if os.Getenv("PRINT_CURL") != "" {
		fmt.Fprintf(os.Stderr, "%v", command)
	}
	// Add some required headers
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("PLANISPHERE-REPORT-KEY", key)
	req.Header.Add("User-Agent", "planisphere-report-go")
	resp, err := c.Do(req)
	if err != nil {
		return nil, err
	}
	defer dclose(resp.Body)
	body, _ := io.ReadAll(resp.Body)

	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		var pay SelfReportTestPayload
		err := json.Unmarshal(body, &pay)
		if err != nil {
			return nil, err
		}
		return &pay, nil
	}
	fmt.Fprintf(os.Stderr, "%+v", string(body))
	return nil, fmt.Errorf("error making request to post data to planisphere 😢")
}

// Summarize returns a string summary of the payload. This should be used where
// you want to print the info, but not necessarily the hundreds of lines of
// installed software
func (p SelfReportPayload) Summarize() string {
	summaryText := strings.Builder{}
	// In normal mode, just show a summary
	e := reflect.ValueOf(&p.Data).Elem()
	for i := 0; i < e.NumField(); i++ {
		varName := e.Type().Field(i).Name
		varValue := e.Field(i).Interface()
		switch name := varName; name {
		case "InstalledSoftware":
			summaryText.WriteString(fmt.Sprintf("%v: %v items\n", varName, len(varValue.([][]string))))
		default:
			summaryText.WriteString(fmt.Sprintf("%v: %v\n", varName, varValue))
		}
	}
	summaryText.WriteString(fmt.Sprintf("ExtraData: %+v\n", p.ExtraData))

	return summaryText.String()
}
