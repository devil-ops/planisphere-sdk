package planisphere

import (
	"context"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestDevicesList(t *testing.T) {
	got, _, err := c.Device.List(context.TODO(), &DeviceParams{})
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestDevicesGet(t *testing.T) {
	got, _, err := c.Device.Get(context.TODO(), 1, &DeviceParams{})
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestDevicesGetVulns(t *testing.T) {
	got, _, err := c.Device.VulnerabilityDetections(context.TODO(), 1, &DeviceParams{})
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestDevicesGetVulnsErr(t *testing.T) {
	tsrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(429)
	}))
	tc := NewClient("", "", nil)
	u, err := url.Parse(tsrv.URL)
	panicIfErr(err)
	tc.BaseURL = u

	got, _, err := tc.Device.VulnerabilityDetections(context.TODO(), 10, &DeviceParams{})
	require.Error(t, err)
	require.Nil(t, got)
}

func TestDevicesGetsErr(t *testing.T) {
	tsrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(429)
	}))
	tc := NewClient("", "", nil)
	u, err := url.Parse(tsrv.URL)
	panicIfErr(err)
	tc.BaseURL = u

	got, _, err := tc.Device.Get(context.TODO(), 1, &DeviceParams{})
	require.Error(t, err)
	require.Nil(t, got)
}
