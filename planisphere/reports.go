package planisphere

import (
	"context"
	"fmt"
	"strings"
	"sync"

	"github.com/spf13/cobra"
)

// ReportService is the interface for methods interacting with the report endpoint
type ReportService interface {
	// Maybe this should be called 'Build', as we are building it out
	Generate(context.Context, *ReportParams) (*DeviceList, *Response, error)
	// This can be a URL copy/paste'd from the 'API Equivalent' link at the bottom of the planisphere devices listing
	Get(context.Context, string) (*DeviceList, *Response, error)
}

// ReportServiceOp is the operator for said service
type ReportServiceOp struct {
	client *Client
}

// ReportParams is the possible parameters to be passed along when looking at a report
type ReportParams struct {
	ActiveSince             int                         `url:"active_since,omitempty"`
	DepartmentKeys          []string                    `url:"department_key[],omitempty"`
	Hostname                string                      `url:"hostname,omitempty"`
	Names                   string                      `url:"names,omitempty"`
	OsNames                 []string                    `url:"os_name[],omitempty"`
	SupportGroupIDs         []float64                   `url:"support_group_id[],omitempty"`
	QuarantineRisks         []string                    `url:"quarantine_risks[],omitempty"`
	VRFs                    []string                    `url:"vrfs[],omitempty"`
	User                    string                      `url:"user,omitempty"`
	VulnerabilitySeverities *VulnerabilityCriticalities `url:"vulnerability_severity[],omitempty"`
	IncludeVulnerablityData bool                        `url:"-"`
	Actionability           Actionability               `url:"-"`
	Risk                    Risk                        `url:"-"`
	ResponsibilityType      string                      `url:"-"`
	HostnameFilters         []string                    `url:"-"`
	VulnFilters             []string                    `url:"-"`
}

// anyg string is just a generic any string. Not to be confused with the interface{}
const anyg string = "any"

func riskWithCmd(cmd cobra.Command) (Risk, error) {
	risk, err := cmd.Flags().GetString("risk")
	if err != nil {
		return AnyRisk, err
	}
	switch risk {
	case anyg:
		return AnyRisk, nil
	case "accepted":
		return AcceptedRisk, nil
	case "unaccepted":
		return UnAcceptedRisk, nil
	default:
		return AnyRisk, fmt.Errorf("unknown type of risk: %v", RiskHelp)
	}
}

func responsibilityWithCmd(cmd cobra.Command) (Responsibility, error) {
	resp, err := cmd.Flags().GetString("responsibility")
	if err != nil {
		return AnyResponsibility, err
	}
	switch resp {
	case anyg:
		return AnyResponsibility, nil
	case "os":
		return OSResponsibility, nil
	case "app":
		return AppResponsibility, nil
	default:
		return AnyResponsibility, fmt.Errorf("unknown type of risk: %v", ResponsibilityHelp)
	}
}

func actionabilityWithCmd(cmd cobra.Command) (Actionability, error) {
	act, err := cmd.Flags().GetString("actionability")
	if err != nil {
		return AnyActionability, err
	}
	switch act {
	case anyg:
		return AnyActionability, nil
	case "actionable":
		return Actionable, nil
	case "unactionable":
		return UnActionable, nil
	default:
		return AnyActionability, fmt.Errorf("unknown type of actionability: %v", ActionabilityHelp)
	}
}

// WithCmd uses options from a cobra.Command to set up the report
func WithCmd(cmd cobra.Command) func(*ReportParams) {
	actionability, err := actionabilityWithCmd(cmd)
	if err != nil {
		actionability = Actionable
	}

	risk, err := riskWithCmd(cmd)
	if err != nil {
		risk = UnAcceptedRisk
	}

	responsibility, err := responsibilityWithCmd(cmd)
	if err != nil {
		responsibility = AnyResponsibility
	}

	var hostFilters []string
	if cmd.Flags().Changed("exclude-host") {
		hostFilters, _ = cmd.Flags().GetStringArray("exclude-host")
	}
	var vulnFilters []string
	if cmd.Flags().Changed("exclude-vuln") {
		vulnFilters, _ = cmd.Flags().GetStringArray("exclude-vuln")
	}

	var name string
	if cmd.Flags().Changed("match-hostnames") {
		name, _ = cmd.Flags().GetString("match-hostnames")
	}

	return func(r *ReportParams) {
		if name != "" {
			r.Names = name
		}
		if len(vulnFilters) > 0 {
			r.VulnFilters = vulnFilters
		}
		if len(hostFilters) > 0 {
			r.HostnameFilters = hostFilters
		}
		if responsibility != "" {
			r.ResponsibilityType = responsibility.String()
		}
		r.Actionability = actionability
		r.Risk = risk
	}
}

// WithActiveSince sets the active_since parameter in the request
func WithActiveSince(i int) func(*ReportParams) {
	return func(r *ReportParams) {
		r.ActiveSince = i
	}
}

// WithAcceptedRisk is just accepted Risk stuff
func WithAcceptedRisk(s Risk) func(*ReportParams) {
	return func(r *ReportParams) {
		r.Risk = s
	}
}

// WithResponsibilityType filters by responsibility_type
func WithResponsibilityType(t string) func(*ReportParams) {
	return func(r *ReportParams) {
		r.ResponsibilityType = t
	}
}

// WithVulnerabilityData will include vulnerability data with all the hosts
func WithVulnerabilityData() func(*ReportParams) {
	return func(r *ReportParams) {
		r.IncludeVulnerablityData = true
	}
}

// NewReportParams returns a new DeviceParams object with some good defaults
func (c *Client) NewReportParams(opts ...func(*ReportParams)) *ReportParams {
	var err error
	p := &ReportParams{
		ActiveSince: 1,
	}
	for _, o := range opts {
		o(p)
	}
	// Figure out some scoping bits
	switch c.supportGroupScope {
	case SGScopeMine:
		c.scopedSupportGroups, _, err = c.SupportGroup.Mine(context.TODO())
		if err != nil {
			panic(err)
		}
		p.SupportGroupIDs = c.scopedSupportGroups.IDs()
	case SGScopeSelected:
		p.SupportGroupIDs = c.scopedSupportGroups.IDs()
		// Nothing actually needed here, it's the default
	case SGScopeAll:
		// Nothing actually needed here, add groups to scope elsewhere
	default:
		panic("unhandled support group scope")
	}
	return p
}

// Generate returns a DeviceList of items matching the given ReportParams
func (svc *ReportServiceOp) Generate(ctx context.Context, params *ReportParams) (*DeviceList, *Response, error) {
	// We need to add the correct columns in here, based off of things set in ReportParams, before passing the params below
	req := mustNewGetRequest(fmt.Sprintf("%s%s/report?%s", svc.client.BaseURL, devicePath, mustQueryValues(params)))
	r := &DeviceList{}
	resp := &Response{}
	var err error
	resp.Response, err = svc.client.sendRequest(req, &r) // nolint:golint,bodyclose
	if err != nil {
		return nil, resp, err
	}
	dclose(resp.Response.Body)

	var prefilteredDevices DeviceList
	if len(params.HostnameFilters) == 0 {
		prefilteredDevices = *r
	} else {
		for _, dev := range *r {
			keep := true
			for _, filter := range params.HostnameFilters {
				if strings.Contains(strings.ToLower(dev.PrimaryName), strings.ToLower(filter)) {
					keep = false
				}
			}
			if keep {
				prefilteredDevices = append(prefilteredDevices, dev)
			}
		}
	}

	// Do we want vuln data up in here too?
	if params.IncludeVulnerablityData {
		return svc.addVulns(ctx, &prefilteredDevices, params)
	}
	return &prefilteredDevices, resp, nil
}

// addVulns returns a new list of items with Vulnerability data added
func (svc *ReportServiceOp) addVulns(ctx context.Context, d *DeviceList, p *ReportParams) (*DeviceList, *Response, error) {
	rv := make(DeviceList, len(*d))
	var wg sync.WaitGroup
	wg.Add(len(*d))
	// Go easy on the API connections here
	guard := make(chan struct{}, maxConnections)
	for idx, d := range *d {
		idx := idx
		guard <- struct{}{}
		go func(d Device) {
			defer wg.Done()
			// fmt.Fprintf(os.Stderr, "RISK: %v\n", p.Risk)
			vulns, _, err := svc.client.Device.VulnerabilityDetections(ctx, d.ID, &DeviceParams{
				// Actionable: p.Actionable,
				Actionability:       p.Actionability,
				Risk:                p.Risk,
				ResponsibilityType:  p.ResponsibilityType,
				ShowVulnerabilities: true,
			})
			panicIfErr(err)

			// Filter vulns
			prefilteredVulns := make(VulnerabilityList, len(*vulns))
			for _, vuln := range *vulns {
				keep := true
				for _, filter := range p.VulnFilters {
					if strings.Contains(strings.ToLower(vuln.Description), strings.ToLower(filter)) {
						keep = false
					}
				}
				if keep {
					prefilteredVulns = append(prefilteredVulns, vuln)
				}
			}

			d.VulnerabilityDetections = &prefilteredVulns
			rv[idx] = d
			<-guard
		}(d)
	}
	wg.Wait()
	return &rv, nil, nil
}

// Get returns a report using the url query parameters to /report
func (svc *ReportServiceOp) Get(_ context.Context, report string) (*DeviceList, *Response, error) {
	req := mustNewGetRequest(fmt.Sprintf("%s%s/report?%s", svc.client.BaseURL, devicePath, report))
	var err error
	r := &DeviceList{}
	resp := &Response{}

	resp.Response, err = svc.client.sendRequest(req, &r) //nolint:golint,bodyclose
	if err != nil {
		return nil, resp, err
	}
	err = resp.Response.Body.Close()
	if err != nil {
		return nil, resp, err
	}
	return r, resp, nil
}

func boolPTR(b bool) *bool {
	return &b
}
