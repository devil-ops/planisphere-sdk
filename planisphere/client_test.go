package planisphere

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

var (
	srv *httptest.Server
	c   *Client
)

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	shutdown()
	os.Exit(code)
}

func shutdown() {
	srv.Close()
}

func testCopyFile(f string, w io.Writer) {
	rp, err := os.ReadFile(f)
	panicIfErr(err)
	_, err = io.Copy(w, bytes.NewReader(rp))
	panicIfErr(err)
}

func setup() {
	srv = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch {
		case strings.HasSuffix(r.URL.Path, "/api/v1/departments"):
			testCopyFile("./testdata/departments.json", w)
			return
		case strings.HasSuffix(r.URL.Path, "/api/v1/devices"):
			testCopyFile("./testdata/devices/list.json", w)
			return
		case strings.HasSuffix(r.URL.Path, "/api/v1/devices/report"):
			testCopyFile("./testdata/devices/list.json", w)
			return
		case strings.HasSuffix(r.URL.Path, "/api/v1/devices/1"):
			testCopyFile("./testdata/devices/get.json", w)
			return
		case strings.HasSuffix(r.URL.Path, "/api/v1/subnets"):
			testCopyFile("./testdata/subnets.json", w)
			return
		case strings.HasSuffix(r.URL.Path, "/api/v1/support_groups"):
			testCopyFile("./testdata/support_groups.json", w)
			return
		case strings.HasSuffix(r.URL.Path, "/api/v1/devices/1/vulnerability_detections"):
			testCopyFile("./testdata/devices/vulns.json", w)
			return
		case strings.HasSuffix(r.URL.Path, "/api/v1/vulnerability_scan_exemptions/static_ips"):
			testCopyFile("./testdata/exemptions/static_ips.json", w)
			return
		case strings.HasSuffix(r.URL.Path, "/api/v1/vulnerability_scan_exemptions/subnets"):
			testCopyFile("./testdata/exemptions/subnets.json", w)
			return
		case strings.HasSuffix(r.URL.Path, "/api/v1/vulnerability_scan_exemptions/wireless_subnets"):
			testCopyFile("./testdata/exemptions/wireless_subnets.json", w)
			return
		case strings.HasSuffix(r.URL.Path, "/api/v1/vulnerability/scan_policies/1/recent_ips"):
			testCopyFile("./testdata/scanpolicies/recent_ips.json", w)
			return
		default:
			panic(fmt.Sprintf("Unexpected location: %v", r.URL.String()))
		}
	}))
	/*
		c = NewClient("", "", nil)
		u, err := url.Parse(srv.URL)
		panicIfErr(err)
		c.BaseURL = u
	*/
	c = MustNew(
		WithUsername("test-user"),
		WithToken("test-token"),
		WithBaseURL(srv.URL),
	)
}

func TestNewClient(t *testing.T) {
	got := NewClient("", "", nil)
	require.NotNil(t, got)
}

func TestSendRequest500Err(t *testing.T) {
	tsrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(500)
	}))
	tc := NewClient("", "", nil)
	u, err := url.Parse(tsrv.URL)
	panicIfErr(err)
	tc.BaseURL = u

	got, _, err := tc.Subnet.List(context.TODO())
	require.Error(t, err)
	require.EqualError(t, err, "unknown error, status code: 500")
	require.Nil(t, got)
}

func TestSendRequest429Err(t *testing.T) {
	tsrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(429)
	}))
	t.Setenv("PRINT_CURL", "1")
	tc := NewClient("", "", nil)
	u, err := url.Parse(tsrv.URL)
	panicIfErr(err)
	tc.BaseURL = u

	got, _, err := tc.Subnet.List(context.TODO())
	require.Error(t, err)
	require.EqualError(t, err, "too many requests.  Check rate limit and make sure the userAgent is set right")
	require.Nil(t, got)
}

func TestSendRequestBadRequest(t *testing.T) {
	req, _ := http.NewRequest("GET", "this is not a url", nil)
	var thing string
	_, err := c.sendRequest(req, &thing) // nolint:golint,bodyclose
	require.Error(t, err)
}

func TestNewClientCurl(t *testing.T) {
	t.Setenv("PRINT_CURL", "1")
	tc := NewClient("", "", nil)
	require.NotNil(t, tc)
}

func TestNewClientFunctional(t *testing.T) {
	nc := MustNew(
		WithUsername("foo"),
		WithToken("bar"),
	)
	require.NotNil(t, nc)
}

func TestNewClientWithEnv(t *testing.T) {
	t.Run("test-no-vars", func(t *testing.T) {
		os.Clearenv()
		require.PanicsWithValue(
			t,
			"no username found in env in either PLANISPHERE_USERNAME or PLANISPHERE_USER",
			func() { New(WithEnvAuth()) },
		)
	})
	t.Run("test-no-token", func(t *testing.T) {
		os.Clearenv()
		t.Setenv("PLANISPHERE_USERNAME", "foo")
		require.PanicsWithValue(
			t,
			"no token found in env PLANISPHERE_TOKEN",
			func() { New(WithEnvAuth()) },
		)
	})
	t.Run("test-working", func(t *testing.T) {
		os.Clearenv()
		t.Setenv("PLANISPHERE_USERNAME", "foo")
		t.Setenv("PLANISPHERE_TOKEN", "bar")
		require.NotPanics(
			t,
			func() { New(WithEnvAuth()) },
		)
	})
}
