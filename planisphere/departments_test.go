package planisphere

import (
	"context"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestDepartmentList(t *testing.T) {
	got, _, err := c.Department.List(context.TODO())
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Equal(t, 2, len(*got))
}

func TestDepartmentListErr(t *testing.T) {
	tsrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(429)
	}))
	tc := NewClient("", "", nil)
	u, err := url.Parse(tsrv.URL)
	panicIfErr(err)
	tc.BaseURL = u

	got, _, err := tc.Department.List(context.TODO())
	require.Error(t, err)
	require.Nil(t, got)
}
