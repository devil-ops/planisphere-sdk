package planisphere

// ResponsibilityHelp describes how to use responsibilities
const ResponsibilityHelp = `This should be one of: "any", "os", "app"`

// RiskHelp describes the type of risk
const RiskHelp = `This should be one of: "any", "accepted", "unaccepted"`

// ActionabilityHelp describes the actionability bits
const ActionabilityHelp = `This should be one of: "any", "actionable", "unactionable"`
