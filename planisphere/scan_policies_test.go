package planisphere

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestScanPolicyRecentIPs(t *testing.T) {
	require.NotNil(t, c.ScanPolicy)
	got, _, err := c.ScanPolicy.RecentIPs(context.Background(), 1)
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Equal(t, &RecentIPs{"1.1.1.1", "1.2.3.5"}, got)
}

func TestRecentIPStrings(t *testing.T) {
	require.Equal(t,
		[]string{
			"1.2.3.4", "1.2.3.5",
		},
		RecentIPs{"1.2.3.4", "1.2.3.5"}.Strings(),
	)
}
