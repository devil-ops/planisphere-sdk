package planisphere

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestValidPayload(t *testing.T) {
	tests := map[string]struct {
		payload SelfReportPayload
		wantErr string
	}{
		"empty": {
			payload: SelfReportPayload{},
			wantErr: "must set either serial or mac_addresses",
		},
		"invalid-device-type": {
			payload: SelfReportPayload{
				Data: SelfReportPayloadData{
					Serial:     "foo",
					DeviceType: "Not-a-real-thing is not a valid device type",
				},
				LastActive: time.Now(),
			},
			wantErr: "Not-a-real-thing is not a valid device type is not a valid device type",
		},
		"valid-device-type": {
			payload: SelfReportPayload{
				Data: SelfReportPayloadData{
					Serial:     "foo",
					DeviceType: "desktop",
				},
				LastActive: time.Now(),
			},
		},
		"invalid-usagetype": {
			payload: SelfReportPayload{
				Data: SelfReportPayloadData{
					Serial:    "foo",
					UsageType: "not-a-real-thing",
				},
				LastActive: time.Now(),
			},
			wantErr: "not-a-real-thing is not a valid usage type",
		},
		"valid": {
			payload: SelfReportPayload{
				Data: SelfReportPayloadData{
					Serial:    "foo",
					UsageType: "server",
				},
				LastActive: time.Now(),
			},
		},
		"invalid-status": {
			payload: SelfReportPayload{
				Data: SelfReportPayloadData{
					Serial: "foo",
					Status: "Not-a-real-thing",
				},
				LastActive: time.Now(),
			},
			wantErr: "Not-a-real-thing is not a valid status",
		},
		"valid-status": {
			payload: SelfReportPayload{
				Data: SelfReportPayloadData{
					Serial: "foo",
					Status: "deployed",
				},
				LastActive: time.Now(),
			},
		},
		"missing-last-active": {
			payload: SelfReportPayload{
				Data: SelfReportPayloadData{
					Serial: "foo",
				},
			},
			wantErr: "must set last_active",
		},
	}
	for desc, tt := range tests {
		err := tt.payload.Validate()
		if tt.wantErr != "" {
			require.Error(t, err, desc)
			require.EqualError(t, err, tt.wantErr, desc)
		} else {
			require.NoError(t, err, desc)
		}
	}
}

func TestSelfReport(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasSuffix(r.URL.Path, "/self_report") {
			data, err := os.ReadFile("testdata/self_report.json")
			panicIfErr(err)
			_, err = io.Copy(w, bytes.NewReader(data))
			panicIfErr(err)
			return
		}
		panic(fmt.Sprintf("requested path that we aren't mocking: %v", r.URL.Path))
	}))
	defer srv.Close()

	t.Setenv("PLANISPHEREREPORT_URL", fmt.Sprintf("%s/self_report", srv.URL))

	payload := &SelfReportPayload{
		LastActive: time.Now(),
	}
	payload.Data.Serial = "FF8X46"

	err := payload.Submit("c9cbeab7-ee43-4216-b79f-6e6fea1b28d9")
	require.NoError(t, err)
}

func TestSelfReportTest(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasSuffix(r.URL.Path, "/self_report/test") {
			r, err := os.Open("testdata/self_report_test.json")
			require.NoError(t, err)
			_, err = io.Copy(w, r)
			require.NoError(t, err)
			return
		}
		defer r.Body.Close()
	}))
	defer srv.Close()

	t.Setenv("PLANISPHEREREPORT_URL", fmt.Sprintf("%s/self_report/test", srv.URL))

	payload := &SelfReportPayload{
		LastActive: time.Now(),
	}
	payload.Data.Serial = "FF8X46"

	payR, err := payload.SubmitTest("c9cbeab7-ee43-4216-b79f-6e6fea1b28d9")
	require.NoError(t, err)
	require.NotNil(t, payR)

	require.Equal(t, payR.ProcessedRecord.Data.Serial, "FF8X46")
	require.Equal(t, payR.Status, "success")
}

func TestValidateSelfReportKey(t *testing.T) {
	tests := map[string]struct {
		key     string
		wantErr string
	}{
		// Good
		"good": {key: "c9cbeab7-ee43-4216-b79f-6e6fea1b28d9"},
		// Bad
		"wrong-length": {key: "c9cbeab7-ee43-4216-b79f-6e6fea1b28d", wantErr: "key does not match UUID format"},
		"random-kebob": {key: "some-value", wantErr: "key does not match UUID format"},
		"empty":        {key: "", wantErr: "key does not contain a hyphen"},
		"no-hyphens":   {key: "c9cbeab7ee434216b79f6e6fea1b28d9", wantErr: "key does not contain a hyphen"},
		"uppercase":    {key: "24900FE7-8332-4878-A23B-04B45A63CC66", wantErr: "key must be completely lowercase"},
	}
	for desc, tt := range tests {
		err := ValidateSelfReportKey(tt.key)
		if tt.wantErr == "" {
			require.NoError(t, err, desc)
		} else {
			require.Error(t, err, desc)
			require.EqualError(t, err, tt.wantErr)
		}
	}
}

func TestExternalIDUnmarshal(t *testing.T) {
	var got SelfReportPayloadData
	d := `{"serial":"FF8X46","device_type":"desktop","usage_type":"server","status":"deployed","external_id":"foo", "external_os_identifiers": [["foo", "bar"]]}`
	err := json.Unmarshal([]byte(d), &got)
	require.NoError(t, err)
	require.Equal(t, got.ExternalOSIdentifiers, ExternalOSIdentifiers{"foo": "bar"})
}

func TestPayloadSummary(t *testing.T) {
	payload := &SelfReportPayload{
		Data: SelfReportPayloadData{
			Serial: "foo",
		},
	}

	got := payload.Summarize()
	require.Equal(t, `OsFullname: 
OsFamily: 
Serial: foo
DepartmentKey: 
DeviceType: 
DiskEncrypted: false
Hostname: 
InstalledSoftware: 0 items
MacAddresses: []
Manufacturer: 
MemoryMB: 0
Model: 
Status: 
SupportGroupID: 0
SupportGroupName: 
UsageType: 
Username: 
OSExtendedSupport: 
ExternalOSIdentifiers: map[]
ExtraData: map[]
`, got)
}
